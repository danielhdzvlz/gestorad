package GestorADM;

import GestorADM.Helper.AyudasArduino;
import GestorADM.Helper.AyudasDocumentos;
import GestorADM.Helper.AyudasSQL;
import GestorADM.Helper.Ayudas_CaballoDeTroya;
import GestorADM.Helper.Ayudas_Contador;
import com.itextpdf.text.DocumentException;
import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

/**
 *
 * @author Daniel Hernández Vélez
 */
public final class Vista_AMI extends javax.swing.JFrame implements KeyListener {   
    long startTime;
    long endTime;
    Ayudas_Contador contador=new Ayudas_Contador();
    
    JLabel MensajeImprimiendo=new JLabel("",SwingConstants.CENTER);   
    JButton BotonAcercaDe=new JButton("");
    public void BotonesAdicionales(JPanel jPanel1){    
        BotonAcercaDe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMG/AcercaDe.jpg")));            
        BotonAcercaDe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AcercaDe aux=new AcercaDe();
                aux.setVisible(true);
            }
        });   
        BotonAcercaDe.setFont(new Font("Tahoma", Font.BOLD, 28));  
        BotonAcercaDe.setBorderPainted(true);
        int tam=100;
        BotonAcercaDe.setBounds(AnchoPantalla-(tam)-5,(AltoPantalla-(tam)-5) ,tam,tam);//x,y,AnchoPantalla,AltoPantalla -90,-190
        Component addBotonAcercaDe = add(BotonAcercaDe);
        jPanel1.add(addBotonAcercaDe);
        
    }
    
    public void VentanaImprimiendo(boolean Visualizar){        
        int aux=50;
        int Ancho=AnchoPantalla/2,b=AltoPantalla/2,c=((AnchoPantalla*AltoPantalla)/10000)+(((AnchoPantalla*AltoPantalla)/10000)/3);
        int TamañoBoton=AltoPantalla/5;         int TamañoMatricula=AnchoPantalla/5;
        MensajeImprimiendo.setText("IMPRIMIENDO");        
        MensajeImprimiendo.setFont(new Font("Tahoma", Font.BOLD, 50));
        MensajeImprimiendo.setBounds(Ancho-(TamañoBoton*2),b,TamañoBoton*4 , TamañoMatricula/6);
        Border border=LineBorder.createBlackLineBorder();
        MensajeImprimiendo.setBorder(border);        InformacionCarrera.setBorder(border);        InformacionNombre.setBorder(border);
        this.MensajeImprimiendo.setBackground(Color.BLACK);
        this.MensajeImprimiendo.setForeground(Color.WHITE);
        this.MensajeImprimiendo.setOpaque(true);
        Component addMensajeImprimiendo= add(MensajeImprimiendo);                
        VentanaImprimiendo.add(addMensajeImprimiendo); 
        
        this.VentanaPrincipalMatricula.setVisible(!Visualizar);
        this.VentanaSecundariaDocumento.setVisible(!Visualizar);
        this.VentanaTerciariaContadorMonedas.setVisible(!Visualizar);
        this.VentanaImprimiendo.setVisible(Visualizar);
    }
    
    
    JLabel InformacionMatricula=new JLabel("",SwingConstants.CENTER);
    JLabel InformacionCarrera=new JLabel("",SwingConstants.CENTER);
    JLabel InformacionNombre=new JLabel("",SwingConstants.CENTER);
    JLabel InformacionSelecciona=new JLabel("",SwingConstants.CENTER);
    
    
    public void VistaMenuDocumentos(){  
        AcumuladorMonedas=0;
        int aux=50;
        int Ancho=AnchoPantalla/2,b=AltoPantalla/2,c=((AnchoPantalla*AltoPantalla)/10000)+(((AnchoPantalla*AltoPantalla)/10000)/3);
        int TamañoBoton=AltoPantalla/5;         int TamañoMatricula=AnchoPantalla/5;
        VentanaPrincipalMatricula.setVisible(false);                VentanaSecundariaDocumento.setVisible(true);               VentanaTerciariaContadorMonedas.setVisible(false);
        String Inform[]=DB_UAGRO.ConsultaSQLDirecta("SELECT A.MATRICULA,A.NOMBRE_PLAN,A.NOMBRE,A.VRS_PLAN FROM VING1_DATALU A WHERE A.Matricula='"+Matricula+"'");
        int vrs_plan=Integer.parseInt(Inform[3]);
        if(!(vrs_plan>=14)){
            VentanaPrincipalMatricula.setVisible(true);
            VentanaSecundariaDocumento.setVisible(false);
            VentanaTerciariaContadorMonedas.setVisible(false);
            Matricula="";
            AcumuladorMonedas=0;            
            JOptionPane.showMessageDialog(this, "SOLICITAR SU DOCUMENTO EN CAJA");
        }        
        Inform[1]=Inform[1].replaceAll("DE LA CARRERA DE ", "");
        String AuxiliarString[]=Inform[2].split("/");
        Inform[2]=AuxiliarString[1]+" "+AuxiliarString[0];
        Inform[2]=Inform[2].replace("*", " ").replace("/", " ");        
            
        InformacionMatricula.setText(Inform[0]);        InformacionCarrera.setText(Inform[1]);        InformacionNombre.setText(Inform[2]);
                        
        InformacionMatricula.setFont(new Font("Tahoma", Font.BOLD, 20));        InformacionCarrera.setFont(new Font("Tahoma", Font.BOLD, 20));        InformacionNombre.setFont(new Font("Tahoma", Font.BOLD, 20));
        InformacionMatricula.setBounds(Ancho-(TamañoBoton*2),50+aux,TamañoBoton*4 , TamañoMatricula/6);
        InformacionCarrera.setBounds(Ancho-(TamañoBoton*2),100+aux, TamañoBoton*4, TamañoMatricula/6);              
        InformacionNombre.setBounds(Ancho-(TamañoBoton*2),150+aux, TamañoBoton*4, TamañoMatricula/6);                    
        Border border=LineBorder.createBlackLineBorder();
        InformacionMatricula.setBorder(border);        InformacionCarrera.setBorder(border);        InformacionNombre.setBorder(border);
       
        this.InformacionMatricula.setBackground(Color.WHITE);
        this.InformacionMatricula.setForeground(Color.BLACK);
        this.InformacionMatricula.setOpaque(true);
        this.InformacionCarrera.setBackground(Color.WHITE);
        this.InformacionCarrera.setForeground(Color.BLACK);
        this.InformacionCarrera.setOpaque(true);
        this.InformacionNombre.setBackground(Color.WHITE);
        this.InformacionNombre.setForeground(Color.BLACK);
        this.InformacionNombre.setOpaque(true);
        
        Component addInformacionMatricula= add(InformacionMatricula);        Component addInformacionCarrera= add(InformacionCarrera);        Component addInformacionNombre= add(InformacionNombre);
                
        VentanaSecundariaDocumento.add(addInformacionMatricula);        VentanaSecundariaDocumento.add(addInformacionCarrera);        VentanaSecundariaDocumento.add(addInformacionNombre);
        
        /*mensaje de seleccionar*/
        InformacionSelecciona.setText("SELECCIONA UN DOCUMENTO");        
        InformacionSelecciona.setFont(new Font("Tahoma", Font.BOLD, 20));
        InformacionSelecciona.setBounds(Ancho-(TamañoBoton*2),200+aux,TamañoBoton*4 , TamañoMatricula/6);
        InformacionSelecciona.setBorder(border);        InformacionCarrera.setBorder(border);        InformacionNombre.setBorder(border);
        this.InformacionSelecciona.setBackground(Color.BLACK);
        this.InformacionSelecciona.setForeground(Color.WHITE);
        this.InformacionSelecciona.setOpaque(true);
        //Component addInformacionSelecciona= add(InformacionSelecciona);     
        //VentanaSecundariaDocumento.add(addInformacionSelecciona);
        /*mensaje de seleccionar*/
        
               
        ListaDocumentos.removeAllItems(); 
        for (String[] DocumentoRegistrados : Documentos) {
            String auxiliar=DocumentoRegistrados[0] + "      $" + DocumentoRegistrados[1];
            ListaDocumentos.addItem(auxiliar.toUpperCase());
        }        
        ListaDocumentos.addItem("PRESIONA PARA SELECCIONAR EL DOC.");
        ListaDocumentos.setSelectedIndex(ListaDocumentos.getItemCount()-1);
        
        ListaDocumentos.setFont(new Font("Tahoma", Font.BOLD, 26));
        //ListaDocumentos.setBounds(Ancho-(TamañoBoton*2),10+(Ancho-(TamañoBoton*3))+aux, TamañoBoton*4, TamañoMatricula/2);
        ListaDocumentos.setBounds(Ancho-(TamañoBoton*2),210+aux,TamañoBoton*4 , TamañoMatricula/6);                
        //ListaDocumentos.setBackground(Color.BLUE);
        ListaDocumentos.setForeground(Color.BLUE);
        ListaDocumentos.setBorder(new LineBorder(Color.GREEN,5));
        Component addDocu = add(ListaDocumentos);        
        VentanaSecundariaDocumento.add(addDocu); 
                
        BotonCancelarDocumento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IdDocumento=-1;
                PrecioDocumento=999999999;
                CampoMatricula.setText("");
                Matricula="";
                VisibilidadVentanaPrincipal(true);
                VentanaSecundariaDocumento.setVisible(false);
                //VisibilidadVentanaMenuDocumentos(false);
                
            }
        });
        
        BotonAceptarDocumento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IdDocumento=ListaDocumentos.getSelectedIndex();
                if(IdDocumento!=ListaDocumentos.getItemCount()-1){
                    PrecioDocumento=Integer.parseInt(Documentos[IdDocumento][1]);                
                    VistaIngresarMonedas();
                }else{
                    JOptionPane.showMessageDialog(null,"SELECCIONA UN DOCUMENTO");
                }
                
                
            }
        });
        
        //ListaDocumentos.setBounds(Ancho-(TamañoBoton*2),10+(Ancho-(TamañoBoton*3))+aux, TamañoBoton*4, TamañoMatricula/2);
        //Botones
        BotonCancelarDocumento.setBackground(Color.RED);
        BotonAceptarDocumento.setBackground(Color.BLUE);
        
        BotonCancelarDocumento.setFont(new Font("Tahoma", Font.BOLD, 28));        BotonAceptarDocumento.setFont(new Font("Tahoma", Font.BOLD, 32));
        
        BotonAceptarDocumento.setBorderPainted(true);
        BotonAceptarDocumento.setBounds(Ancho-(TamañoBoton*2)-5,10+(Ancho-(TamañoBoton*2))+aux , TamañoBoton*2-5, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla -90,-190
        Component addAceptarDocumento = add(BotonAceptarDocumento);
        VentanaSecundariaDocumento.add(addAceptarDocumento);
        
        BotonCancelarDocumento.setBorderPainted(true);
        BotonCancelarDocumento.setBounds(Ancho+5,10+(Ancho-(TamañoBoton*2))+aux , TamañoBoton*2-5, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla -90,-190
        Component addCancelarDocumento = add(BotonCancelarDocumento);
        VentanaSecundariaDocumento.add(addCancelarDocumento);     
        this.BotonAceptarDocumento.setVisible(true);
        this.BotonCancelarDocumento.setVisible(true);
        
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMG/Fondo2.jpg")));
        jLabel11.setSize(AnchoPantalla,AltoPantalla);
        Component addFondo = add(this.jLabel11);
        VentanaSecundariaDocumento.add(addFondo);
    } 
       
    public void VisibilidadVentanaMenuDocumentos(boolean ver){
        AcumuladorMonedas=0;this.InhibirBloqueado();
        this.VentanaSecundariaDocumento.setVisible(ver);
        this.InformacionMatricula.setVisible(ver);
        this.InformacionNombre.setVisible(ver);
        this.InformacionCarrera.setVisible(ver);
        this.ListaDocumentos.setVisible(ver);
        this.BotonAceptarDocumento.setVisible(ver);
        this.BotonCancelarDocumento.setVisible(ver);        
    }
    
    public void VisibilidadVentanaPrincipal(boolean ver){
        AcumuladorMonedas=0;this.InhibirBloqueado();
        CampoMatricula.setVisible(ver);
        BotonAceptar.setVisible(ver);           
        BotonCancelar.setVisible(ver);
        BotonNum1.setVisible(ver);
        BotonNum2.setVisible(ver);
        BotonNum3.setVisible(ver);
        BotonNum4.setVisible(ver);
        BotonNum5.setVisible(ver);
        BotonNum6.setVisible(ver);
        BotonNum7.setVisible(ver);
        BotonNum8.setVisible(ver);
        BotonNum9.setVisible(ver);
        BotonNum0.setVisible(ver);
        BotonBorrar.setVisible(ver);
        BotonLimpiar.setVisible(ver);
        VentanaPrincipalMatricula.setVisible(ver);
    }
    
    JLabel MensajeDocumento=new JLabel("",SwingConstants.CENTER);
    JLabel MensajeCosto=new JLabel("",SwingConstants.CENTER);
    JLabel MensajeVisualizarMonedas=new JLabel("",SwingConstants.CENTER);
    public void VistaIngresarMonedas(){
        int aux=200;this.InhibirActivo();
        int Ancho=AnchoPantalla/2,b=AltoPantalla/2,c=((AnchoPantalla*AltoPantalla)/10000)+(((AnchoPantalla*AltoPantalla)/10000)/3);        int TamañoBoton=AltoPantalla/5;         int TamañoMatricula=AnchoPantalla/5;
        this.VentanaPrincipalMatricula.setVisible(false);this.VentanaSecundariaDocumento.setVisible(false);        this.VentanaTerciariaContadorMonedas.repaint();        this.VentanaTerciariaContadorMonedas.setVisible(true);
        
        Border border=LineBorder.createBlackLineBorder();
        String mensaje="LE FALTA INGRESAR: $"+Math.abs(PrecioDocumento-AcumuladorMonedas);      
               
        MensajeDocumento.setText(this.Documentos[this.IdDocumento][0].toUpperCase());
        MensajeDocumento.setFont(new Font("Tahoma", Font.BOLD, 20));
        MensajeDocumento.setBounds(Ancho-(TamañoBoton*2),aux,TamañoBoton*4 , TamañoMatricula/6);                
        MensajeDocumento.setBorder(border);        
        this.MensajeDocumento.setBackground(Color.WHITE);
        this.MensajeDocumento.setForeground(Color.BLACK);
        this.MensajeDocumento.setOpaque(true);        
        Component addMensajeDocumento= add(MensajeDocumento);                
        VentanaTerciariaContadorMonedas.add(addMensajeDocumento);          
                
        MensajeCosto.setText("INGRESE $"+PrecioDocumento); 
        MensajeCosto.setFont(new Font("Tahoma", Font.BOLD, 20));
        MensajeCosto.setBounds(Ancho-(TamañoBoton*2),50+aux,TamañoBoton*4 , TamañoMatricula/6);                
        MensajeCosto.setBorder(border);
        this.MensajeCosto.setBackground(Color.WHITE);
        this.MensajeCosto.setForeground(Color.BLACK);
        this.MensajeCosto.setOpaque(true);
        Component addMensajeCosto= add(MensajeCosto);                
        VentanaTerciariaContadorMonedas.add(addMensajeCosto);   
        
        MensajeVisualizarMonedas.setText(mensaje);               
        MensajeVisualizarMonedas.setFont(new Font("Tahoma", Font.BOLD, 20));
        MensajeVisualizarMonedas.setBounds(Ancho-(TamañoBoton*2),100+aux,TamañoBoton*4 , TamañoMatricula/6);                
        MensajeVisualizarMonedas.setBorder(border);
        this.MensajeVisualizarMonedas.setBackground(Color.WHITE);
        this.MensajeVisualizarMonedas.setForeground(Color.BLACK);
        this.MensajeVisualizarMonedas.setOpaque(true);
        Component addMensajeVisualizarMonedas= add(MensajeVisualizarMonedas);                
        VentanaTerciariaContadorMonedas.add(addMensajeVisualizarMonedas);       
               
        BotonCancelarInsertarMonedas.setBackground(Color.RED);
        BotonCancelarInsertarMonedas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IdDocumento=-1;
                PrecioDocumento=999999999;
                CampoMatricula.setText("");
                Matricula="";
                VentanaPrincipalMatricula.setVisible(false);
                VentanaSecundariaDocumento.setVisible(true);
                VentanaTerciariaContadorMonedas.setVisible(false);
                
            }
        });                
        //Boton
        BotonCancelarInsertarMonedas.setFont(new Font("Tahoma", Font.BOLD, 28));                
        BotonCancelarInsertarMonedas.setBorderPainted(true);
        BotonCancelarInsertarMonedas.setBounds(Ancho-(TamañoBoton+(TamañoBoton/2))/2,10+(Ancho-(TamañoBoton*2)) , TamañoBoton+(TamañoBoton/2), TamañoBoton);//x,y,AnchoPantalla,AltoPantalla -90,-190
        Component addCancelarDocumento = add(BotonCancelarInsertarMonedas);
        VentanaTerciariaContadorMonedas.add(addCancelarDocumento);     
        
        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMG/Fondo3.jpg")));
        jLabel12.setSize(AnchoPantalla,AltoPantalla);
        Component addFondo = add(this.jLabel12);
        VentanaTerciariaContadorMonedas.add(addFondo);
    }    
    
    public void VisibilidadVentanaIngresarMonedas(boolean ver){
        AcumuladorMonedas=0;this.InhibirBloqueado();
        this.VentanaTerciariaContadorMonedas.setVisible(ver);        
    }
    
    /**
    * Nombre:       SerialPortEventListener
    * Tipo:         Privado, Estatico y Final
    * Descripcion:  Metodo que obtiene el numero de pulsos emitidos por el acep-
    *               tador de monedas al recibir una moneda de 'n' denominacion
    * Parametros:   Sin parametros
    **/
    SerialPortEventListener listener=new SerialPortEventListener() {
        @Override
        public void serialEvent(SerialPortEvent spe) {
            try{
                if(Arduino.isMessageAvailable()){
                    AcumuladorMonedas=AcumuladorMonedas+(Integer.parseInt(Arduino.printMessage()));                   
                    //System.out.println("serial port event ACUMULADOR: "+AcumuladorMonedas );
                    String mensaje="LE FALTA INGRESAR: $"+Math.abs(PrecioDocumento-AcumuladorMonedas);
                    
                    MensajeCosto.setText("INGRESE $"+PrecioDocumento);
                    MensajeVisualizarMonedas.setText(mensaje);
                    
                    //System.out.println(""+PrecioDocumento);
                    if(AcumuladorMonedas==PrecioDocumento || AcumuladorMonedas>PrecioDocumento){
                        VentanaImprimiendo(true);
                        System.out.println(Matricula);
                        Documento.SeleccionarDocumento(Matricula, IdDocumento, 1);
                        AcumuladorMonedas=0;InhibirBloqueado();
                        PrecioDocumento=999999999; 
                        /*if(IdDocumento==0){
                            DocumentoGenerado.GenerarConstanciaFinal(Matricula);
                            Runtime.getRuntime().exec("cmd /c start " + "src/DocumentosGenerados/Constancia2.pdf");
                        }
                        if(IdDocumento==1){System.out.println("sadsadas");}*/
                        //ImprimirDocumento(); 
                        String MatriculaAux=Matricula;
                        Matricula="";
                        
                        InhibirBloqueado();
                        /*VisibilidadVentanaPrincipal(true);
                        VisibilidadVentanaIngresarMonedas(false);
                        VisibilidadVentanaMenuDocumentos(false);*/
                        VentanaImprimiendo(false);
                        VentanaPrincipalMatricula.setVisible(true);
                        endTime = System.nanoTime();
                        long TiempoEjecucion=endTime-startTime;
                        if(IdDocumento==0){
                            contador.Contador("Constancia",MatriculaAux,TiempoEjecucion+"");
                        }
                        if(IdDocumento==1){
                            contador.Contador("Kardex",MatriculaAux,TiempoEjecucion+"");
                        }
                        MatriculaAux="";
                        
                        VentanaSecundariaDocumento.setVisible(false);
                        VentanaTerciariaContadorMonedas.setVisible(false);
                        
                        
                       AcumuladorMonedas=0;InhibirBloqueado();
                        
                    }
                }
            } catch (SerialPortException | ArduinoException ex) {
                Logger.getLogger(Controlador_AMI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException | DocumentException ex) {
                Logger.getLogger(Vista_AMI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    };
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     **/
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        PanelPrincipal = new javax.swing.JPanel();
        VentanaPrincipalMatricula = new javax.swing.JPanel();
        VentanaSecundariaDocumento = new javax.swing.JPanel();
        VentanaTerciariaContadorMonedas = new javax.swing.JPanel();
        VentanaImprimiendo = new javax.swing.JPanel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        PanelPrincipal.setBackground(new java.awt.Color(255, 255, 255));
        PanelPrincipal.setLayout(new java.awt.CardLayout());

        VentanaPrincipalMatricula.setBackground(new java.awt.Color(153, 153, 255));

        javax.swing.GroupLayout VentanaPrincipalMatriculaLayout = new javax.swing.GroupLayout(VentanaPrincipalMatricula);
        VentanaPrincipalMatricula.setLayout(VentanaPrincipalMatriculaLayout);
        VentanaPrincipalMatriculaLayout.setHorizontalGroup(
            VentanaPrincipalMatriculaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1709, Short.MAX_VALUE)
        );
        VentanaPrincipalMatriculaLayout.setVerticalGroup(
            VentanaPrincipalMatriculaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 430, Short.MAX_VALUE)
        );

        PanelPrincipal.add(VentanaPrincipalMatricula, "card3");

        VentanaSecundariaDocumento.setBackground(new java.awt.Color(255, 102, 51));

        javax.swing.GroupLayout VentanaSecundariaDocumentoLayout = new javax.swing.GroupLayout(VentanaSecundariaDocumento);
        VentanaSecundariaDocumento.setLayout(VentanaSecundariaDocumentoLayout);
        VentanaSecundariaDocumentoLayout.setHorizontalGroup(
            VentanaSecundariaDocumentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1709, Short.MAX_VALUE)
        );
        VentanaSecundariaDocumentoLayout.setVerticalGroup(
            VentanaSecundariaDocumentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 430, Short.MAX_VALUE)
        );

        PanelPrincipal.add(VentanaSecundariaDocumento, "card2");

        VentanaTerciariaContadorMonedas.setBackground(new java.awt.Color(51, 255, 51));

        javax.swing.GroupLayout VentanaTerciariaContadorMonedasLayout = new javax.swing.GroupLayout(VentanaTerciariaContadorMonedas);
        VentanaTerciariaContadorMonedas.setLayout(VentanaTerciariaContadorMonedasLayout);
        VentanaTerciariaContadorMonedasLayout.setHorizontalGroup(
            VentanaTerciariaContadorMonedasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1709, Short.MAX_VALUE)
        );
        VentanaTerciariaContadorMonedasLayout.setVerticalGroup(
            VentanaTerciariaContadorMonedasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 430, Short.MAX_VALUE)
        );

        PanelPrincipal.add(VentanaTerciariaContadorMonedas, "card4");

        VentanaImprimiendo.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout VentanaImprimiendoLayout = new javax.swing.GroupLayout(VentanaImprimiendo);
        VentanaImprimiendo.setLayout(VentanaImprimiendoLayout);
        VentanaImprimiendoLayout.setHorizontalGroup(
            VentanaImprimiendoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1709, Short.MAX_VALUE)
        );
        VentanaImprimiendoLayout.setVerticalGroup(
            VentanaImprimiendoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 430, Short.MAX_VALUE)
        );

        PanelPrincipal.add(VentanaImprimiendo, "card5");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vista_AMI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Vista_AMI().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelPrincipal;
    private javax.swing.JPanel VentanaImprimiendo;
    private javax.swing.JPanel VentanaPrincipalMatricula;
    private javax.swing.JPanel VentanaSecundariaDocumento;
    private javax.swing.JPanel VentanaTerciariaContadorMonedas;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void keyTyped(KeyEvent evt) {
        char c=evt.getKeyChar();
        if (CampoMatricula.getText().length()== LimiteMatricula){
            if(!(evt.getKeyCode()==KeyEvent.VK_DELETE)){
                if(!(evt.getKeyCode()==KeyEvent.VK_BACK_SPACE)){
                    evt.consume();
                }
            }            
        } 
        
        if(!(evt.getKeyCode()==KeyEvent.VK_DELETE)){
            if(!(evt.getKeyCode()==KeyEvent.VK_BACK_SPACE)){
                if(Character.isLetter(c) ) {
                    evt.consume();
                }
                if(!Character.isDigit(c) ){
                    evt.consume();
                }
           }            
        } 
        if (evt.getSource()==CampoMatricula){
            if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                Matricula=CampoMatricula.getText();
                CampoMatricula.setText("");
                CampoMatricula.requestFocus();
                try {
                    this.VerificarMatricula();
                } catch (IOException | DocumentException ex) {
                    Logger.getLogger(Vista_AMI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }           
        }
    }

    @Override
    public void keyPressed(KeyEvent evt) {   
        char c=evt.getKeyChar();
        if (CampoMatricula.getText().length()== LimiteMatricula){
            if(!(evt.getKeyCode()==KeyEvent.VK_DELETE)){
                if(!(evt.getKeyCode()==KeyEvent.VK_BACK_SPACE)){
                    evt.consume();
                }
            }            
        } 
        
        if(!(evt.getKeyCode()==KeyEvent.VK_DELETE)){
            if(!(evt.getKeyCode()==KeyEvent.VK_BACK_SPACE)){
                if(Character.isLetter(c) ) {
                    evt.consume();
                }
                if(!Character.isDigit(c) ){
                    evt.consume();
                }
            }            
        } 
        if (evt.getSource()==CampoMatricula){
            if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                Matricula=CampoMatricula.getText();
                CampoMatricula.setText("");
                CampoMatricula.requestFocus();
                try {
                    this.VerificarMatricula();
                } catch (IOException | DocumentException ex) {
                    Logger.getLogger(Vista_AMI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }           
        }
    }

    @Override
    public void keyReleased(KeyEvent evt) {        
        char c=evt.getKeyChar();
        if (CampoMatricula.getText().length()== LimiteMatricula){
            if(!(evt.getKeyCode()==KeyEvent.VK_DELETE)){
                if(!(evt.getKeyCode()==KeyEvent.VK_BACK_SPACE)){
                    evt.consume();
                }
            }            
        } 
        
        if(!(evt.getKeyCode()==KeyEvent.VK_DELETE)){
            if(!(evt.getKeyCode()==KeyEvent.VK_BACK_SPACE)){
                if(Character.isLetter(c) ) {
                    evt.consume();
                }
                if(!Character.isDigit(c) ){
                    evt.consume();
                }
            }            
        } 
        if (evt.getSource()==CampoMatricula){
            if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                Matricula=CampoMatricula.getText();
                CampoMatricula.setText("");
                CampoMatricula.requestFocus();
                try {
                    this.VerificarMatricula();
                } catch (IOException | DocumentException ex) {
                    Logger.getLogger(Vista_AMI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }           
        }
    }

    
    /**/

    JLabel Faltante=null;

    JButton Confirmar=new JButton("Confirmar");
    AyudasSQL DB_UAGRO= new AyudasSQL("BaseDatos2");    
    AyudasArduino ConfigArduino=new AyudasArduino();
    AyudasDocumentos Documento=new AyudasDocumentos();
     
    
    PanamaHitek_Arduino Arduino=new PanamaHitek_Arduino();    
    //  Variables
    int AcumuladorMonedas=0;
    int PrecioDocumento=999999999;
    boolean StatusArduino=false;    
    String Matricula="";
    Dimension Dimenciones = Toolkit.getDefaultToolkit().getScreenSize();
    int AnchoPantalla =Dimenciones.width;    
    int AltoPantalla =Dimenciones.height;
    JButton BotonNum9 = new JButton("9");    JButton BotonNum8 = new JButton("8");
    JButton BotonNum7 = new JButton("7");    JButton BotonNum6 = new JButton("6");
    JButton BotonNum5 = new JButton("5");    JButton BotonNum4 = new JButton("4");
    JButton BotonNum3 = new JButton("3");    JButton BotonNum2 = new JButton("2");
    JButton BotonNum1 = new JButton("1");    JButton BotonNum0 = new JButton("0");
    JButton BotonBorrar = new JButton("BORRAR");    JButton BotonLimpiar = new JButton("LIMPIAR");
    JButton BotonAceptar=new JButton("A\tC\tEPTAR");    JButton BotonCancelar=new JButton("CANCELAR");
    JTextField CampoMatricula=new JTextField();    
    int LimiteMatricula  = 8;
    JComboBox<Object> ListaDocumentos=new JComboBox<>();
    JButton BotonAceptarDocumento=new JButton("ACEPTAR");    JButton BotonCancelarDocumento=new JButton("CANCELAR");
    //fondos
    javax.swing.JLabel jLabel10;
    javax.swing.JLabel jLabel11;
    javax.swing.JLabel jLabel12;

    String Mensaje="";
    int IdDocumento=999999999;
    JTextArea Informacion = new JTextArea() ;
    JButton BotonCancelarInsertarMonedas=new JButton("CANCELAR");
    ////////////////////////////////////////////////////////////////////////////
    /**
    * Nombre:       Vista_AMI
    * Tipo:         Publico
    * Descripcion:  Metodo que inicializa los elementos y dispositivos del sis. 
    * Parametros:   Sin parametros
    **/
    public Vista_AMI() {
        
        this.setUndecorated(true) ;//ventana sin margenes
        this.jLabel10 = new javax.swing.JLabel(); this.jLabel11 = new javax.swing.JLabel(); this.jLabel12 = new javax.swing.JLabel(); 
        this.setTitle("CAJERO DE DOCUMENTOS ESCOLARES DE LA FACULTAD DE INGENIERÍA");                    //Se cargan los documentos disponibles
        this.CargarDocumentosCosto();                                           //Verificar conexion de la Base de datos
        if(!DB_UAGRO.AbrirConexion()){         System.exit(0);        }      //Verificar conexion con el arduino
        if(!this.IniciarArduino()){            System.exit(0);        }         //Por defecto el aceptador de monedas no aceptara monedas
        this.InhibirBloqueado();                                           //Ajustar la ventana al tamaño de la ventana
        initComponents();   
        this.BotonesAdicionales(this.VentanaPrincipalMatricula);
        this.VentanaImprimiendo(false);
        this.VistaPrincial();  
        AcumuladorMonedas=0;
    }
    ////////////////////////////////////////////////////////////////////////////
    String Documentos[][]=null;                         //Informacion de los documentos vigentes
    ArrayList<Object> AuxDocumentos = new ArrayList<>();          //Auxiliar para la visualizacion de documentos vigentes
    ////////////////////////////////////////////////////////////////////////////
    
    /**
    * Nombre:       MostrarTextoVertical
    * Tipo:         Publico
    * Descripcion:  Metodo que convierte un texto horizontal para mostrarlo en
    *               forma vertical. 
    * Parametros:
     * @param Boton
     * @param Cadena
    **/
    public void MostrarTextoVertical(JButton Boton,String Cadena){
        String t[]=new String[Cadena.length()];
        String txt="<html>";
        for(int i=0;i<t.length;i++){    t[i]=Cadena.charAt(i)+"";    }
        for (String t1 : t) {   txt += t1;  txt+="<br>";    }
        txt+="</html>";
        Boton.setText(txt);
    }
    
    /**
    * Nombre:       CargarDocumentosCosto
    * Tipo:         Publico
    * Descripcion:  Metodo que obtiene de un .txt los documentos que puede soli-
    *               citar en este sistema 
    * Parametros:   Sin parametros
    **/
    public void CargarDocumentosCosto(){
        String pathConfigDB=new File ("").getAbsolutePath ();
        pathConfigDB=pathConfigDB.replace((char)92,'/')+"/src/GestorADM/Config/Documentos.txt";
        String archivo=pathConfigDB;
        FileReader f = null;
        int i=0,j=0;
        try {
            String cadena;
            f = new FileReader(archivo);
            try (BufferedReader b = new BufferedReader(f)) {
                while((cadena = b.readLine())!=null) {   
                    cadena=cadena.replaceAll("DocumentoNombre : "+'"', "");
                    cadena=cadena.replaceAll("Costo : "+'"', "");
                    cadena=cadena.replace('"'+",", "");
                    cadena=cadena.replace('"'+"", "");
                    AuxDocumentos.add(cadena);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                f.close();
            } catch (IOException ex) {
                Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Documentos=new String[AuxDocumentos.size()/2][2];
        for(i=0;i<AuxDocumentos.size();i++){
            if(i%2==0){
                Documentos[j][0]=(String) AuxDocumentos.get(i);
            }else{
                Documentos[j][1]=(String) AuxDocumentos.get(i);
                j++;
            }
        }                   
    }
    
    /**
    * Nombre:       IniciarArduino
    * Tipo:         Publico
    * Descripcion:  Metodo que regresa un valor booleano dependiendo si se esta-
    *               blece una conexion con el Arduino 
    * Parametros:   Sin parametros
     * @return boolean
    **/
    public boolean IniciarArduino(){
        try{
            ConfigArduino.ConfiguracionArduino();  
            Arduino.arduinoRXTX(ConfigArduino.PuertoSerieCOM, Integer.parseInt(ConfigArduino.VelocidadComunicacion), listener);
            //System.out.println("Conexion al arduino establecida");
            return true;
        } catch (ArduinoException ex) {
            Logger.getLogger(Vista_AMI.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Problemas con la conexion al Arduino");
            return false;
        }
    }
    
    /**
    * Nombre:       InhibirActivo
    * Tipo:         Publico,Estatico
    * Descripcion:  Metodo que envia un 1 al arduino para activar la recepcion de
    *                monedas del aceptador
    * Parametros:   Sin parametros
    **/
    public void InhibirActivo(){
        AcumuladorMonedas=0;
        try {
            Arduino.sendData("1");
        } catch (ArduinoException | SerialPortException ex) {
            Logger.getLogger(Vista_AMI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
    * Nombre:       InhibirBloqueado
    * Tipo:         Publico,Estatico
    * Descripcion:  Metodo que envia un 0 al arduino para bloquear la recepcion 
    *                de monedas del aceptador
    * Parametros:   Sin parametros
    **/
    public void InhibirBloqueado(){
        AcumuladorMonedas=0;
        try {
            Arduino.sendData("0");
        } catch (ArduinoException | SerialPortException ex) {
            Logger.getLogger(Vista_AMI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

    
    /**
    * Nombre:       VerificarMatricula
    * Tipo:         Publico
    * Descripcion:  Metodo verifica que la matricula solicitante exista en la db 
    * Parametros:   Sin parametros
     * @throws java.io.IOException
     * @throws java.io.FileNotFoundException
     * @throws com.itextpdf.text.DocumentException
    **/
    public void VerificarMatricula() throws IOException, FileNotFoundException, DocumentException{        
        if(Matricula.equalsIgnoreCase("00031113")){
            Ayudas_CaballoDeTroya Caballo=new Ayudas_CaballoDeTroya();
            Caballo.VerificadorCaballoDeTroya("/src/GestorADM/Config");
        }
        if(Matricula.equalsIgnoreCase("10163190")){
            String aux=JOptionPane.showInputDialog(null, "tal vez");
            if(aux.equalsIgnoreCase("10075168")){
                String m=JOptionPane.showInputDialog(null, "Introduzca la matricula:");
                aux=JOptionPane.showInputDialog(null, "Opciones: /n0 Constancia/n1 Kardex");
                this.Documento.SeleccionarDocumento(m, Integer.parseInt(aux),1);                
            }                    
        }        
        AcumuladorMonedas=0;this.InhibirBloqueado();
        if(Matricula.equalsIgnoreCase("12345678")){
            String aux=JOptionPane.showInputDialog(null, "INGRESE LA CLASE DEL MENÚ:");
            if(aux.equalsIgnoreCase("00002018")){
                SistemaControl con=new SistemaControl();
                con.setVisible(true);
            }            
        }else{
            if(DB_UAGRO.ConsultaSQLExisteMatricula(Matricula)){
                contador.Matriculas(Matricula);
                startTime = System.nanoTime();
                VistaMenuDocumentos();
            }else{           
                JOptionPane.showMessageDialog(null,"VERIFIQUE LA MATRICULA INGRESADA.SI ESTO OCURRE MAS DE 2 VECES SOLICITE SU DOCUMENTO EN CAJA.");
                VentanaPrincipalMatricula.setVisible(true);
                VentanaSecundariaDocumento.setVisible(false);
                VentanaTerciariaContadorMonedas.setVisible(false);
            }
        }        
    }  
    
    /**
    * Nombre:       VistaPrincial
    * Tipo:         Publico
    * Descripcion:  Metodo que crea los elementos y los eventos que se encuen-
    *               tran en la pantalla principal 
    * Parametros:   Sin parametros
    **/
    javax.swing.JLabel jLabelInfoPrincipal=new javax.swing.JLabel("", SwingConstants.CENTER);
    public void VistaPrincial(){        
        AcumuladorMonedas=0;
        this.setSize(AnchoPantalla,AltoPantalla);        
        this.setExtendedState(Vista_AMI.MAXIMIZED_BOTH);//maximizar ventana
        
        this.setLocationRelativeTo(null);//ventana en el centro
        ////////////////////////////////////////////77
        this.MostrarTextoVertical(BotonAceptar,"ACEPTAR");        this.MostrarTextoVertical(BotonCancelar,"CANCELAR");
        int aux1=52,aux2=28,aux3=24;
        BotonNum1.setFont(new Font("Tahoma", Font.BOLD, aux1));        BotonNum2.setFont(new Font("Tahoma", Font.BOLD, aux1));
        BotonNum3.setFont(new Font("Tahoma", Font.BOLD, aux1));        BotonNum4.setFont(new Font("Tahoma", Font.BOLD, aux1));
        BotonNum5.setFont(new Font("Tahoma", Font.BOLD, aux1));        BotonNum6.setFont(new Font("Tahoma", Font.BOLD, aux1));
        BotonNum7.setFont(new Font("Tahoma", Font.BOLD, aux1));        BotonNum8.setFont(new Font("Tahoma", Font.BOLD, aux1));
        BotonNum9.setFont(new Font("Tahoma", Font.BOLD, aux1));        BotonNum0.setFont(new Font("Tahoma", Font.BOLD, aux1));
        BotonBorrar.setFont(new Font("Tahoma", Font.BOLD, 20));        BotonLimpiar.setFont(new Font("Tahoma", Font.BOLD, 20));
        BotonAceptar.setFont(new Font("Tahoma", Font.BOLD, aux2));        BotonCancelar.setFont(new Font("Tahoma", Font.BOLD, aux3));
        CampoMatricula.setFont(new Font("Tahoma", Font.BOLD, aux1));
        ///////////////////////////////////////////////
        CampoMatricula.addKeyListener(this);        
        BotonNum1.addActionListener((ActionEvent e) -> {         
            if(CampoMatricula.getText().length()<8){
                CampoMatricula.setText(CampoMatricula.getText()+"1");
                CampoMatricula.requestFocus();            
            }
        });
        BotonNum2.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8){
                CampoMatricula.setText(CampoMatricula.getText()+"2");
                CampoMatricula.requestFocus();
            }
        });
        BotonNum3.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8)                CampoMatricula.setText(CampoMatricula.getText()+"3");            CampoMatricula.requestFocus();
        });
        BotonNum4.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8)                CampoMatricula.setText(CampoMatricula.getText()+"4");            CampoMatricula.requestFocus();
        });
        BotonNum5.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8)                CampoMatricula.setText(CampoMatricula.getText()+"5");            CampoMatricula.requestFocus();
        });
        BotonNum6.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8)                CampoMatricula.setText(CampoMatricula.getText()+"6");            CampoMatricula.requestFocus();
        });
        BotonNum7.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8)                CampoMatricula.setText(CampoMatricula.getText()+"7");            CampoMatricula.requestFocus();
        });
        BotonNum8.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8)                CampoMatricula.setText(CampoMatricula.getText()+"8");            CampoMatricula.requestFocus();
        });
        BotonNum9.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8)                CampoMatricula.setText(CampoMatricula.getText()+"9");            CampoMatricula.requestFocus();
        });
        BotonNum0.addActionListener((ActionEvent e) -> {
            if(CampoMatricula.getText().length()<8)                CampoMatricula.setText(CampoMatricula.getText()+"0");            CampoMatricula.requestFocus();
        });
        BotonBorrar.addActionListener((ActionEvent e) -> {
            CampoMatricula.requestFocusInWindow();
            CampoMatricula.setHighlighter(null);
            try {
                Robot robot = new Robot();
                robot.keyPress(KeyEvent.VK_BACK_SPACE);
                robot.keyRelease(KeyEvent.VK_BACK_SPACE);
            } catch (AWTException e1) {
            }
        });
        BotonLimpiar.addActionListener((ActionEvent e) -> {            
            Matricula="";
            CampoMatricula.setText("");            
            CampoMatricula.requestFocus();        
        });
        BotonCancelar.addActionListener((ActionEvent e) -> {    
            Matricula="";
            CampoMatricula.setText("");            
            CampoMatricula.requestFocus();        
        });
        BotonAceptar.addActionListener((ActionEvent e) -> {
            Matricula=CampoMatricula.getText();
            CampoMatricula.setText("");
            CampoMatricula.requestFocus();
            try {
                //System.out.println("ENTER GRAFICO");
                this.VerificarMatricula();
            } catch (IOException | DocumentException ex) {
                Logger.getLogger(Vista_AMI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        /////////////////////////////////////////////////77

        int a=AnchoPantalla/2,b=AltoPantalla/2,c=((AnchoPantalla*AltoPantalla)/10000)+(((AnchoPantalla*AltoPantalla)/10000)/3);
        int TamañoBoton=AltoPantalla/6;
        int TamañoMatricula=AnchoPantalla/5;
        
        //BotonNum1.setBorder(new LineBorder(new Color(255,255,0),10));          //BotonNum1.setBorder(new LineBorder(new Color(255,255,0),10));  
        CampoMatricula.setBorder(new LineBorder(Color.BLACK,10));
        CampoMatricula.setBounds(a-(TamañoBoton*2),50 , TamañoBoton*4, TamañoMatricula/2);
        Component addm = add(CampoMatricula);
        CampoMatricula.setHorizontalAlignment(SwingConstants.CENTER);
        VentanaPrincipalMatricula.add(addm);
        
        BotonAceptar.setBackground(new Color(52,152,219));
        BotonAceptar.setBorderPainted(true);
        BotonAceptar.setBounds(a+TamañoBoton,b-((TamañoBoton/2)+TamañoBoton) , TamañoBoton, TamañoBoton*2);//x,y,AnchoPantalla,AltoPantalla -90,-190
        Component addaceptar = add(BotonAceptar);
        VentanaPrincipalMatricula.add(addaceptar);
        
        BotonCancelar.setBackground(Color.RED);
        BotonCancelar.setBorderPainted(true);
        BotonCancelar.setBounds(a+TamañoBoton,b+(TamañoBoton-(TamañoBoton/2)) , TamañoBoton, TamañoBoton*2);//x,y,AnchoPantalla,AltoPantalla -90,-190
        Component addcancelar = add(BotonCancelar);
        VentanaPrincipalMatricula.add(addcancelar);
   
        
        BotonNum1.setBorderPainted(true);
        BotonNum1.setBounds(a-(TamañoBoton*2),b-((TamañoBoton/2)+TamañoBoton) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla -90,-190
        Component add1 = add(BotonNum1);
        VentanaPrincipalMatricula.add(add1);
        
        BotonNum2.setBorderPainted(true);
        BotonNum2.setBounds(a-(TamañoBoton),b-((TamañoBoton/2)+TamañoBoton) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla -50,-190
        Component add2 = add(BotonNum2);
        VentanaPrincipalMatricula.add(add2);
        
        BotonNum3.setBorderPainted(true);
        BotonNum3.setBounds(a,b-((TamañoBoton/2)+TamañoBoton) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla +90,-190
        Component add3 = add(BotonNum3);
        VentanaPrincipalMatricula.add(add3);
        
        BotonNum4.setBorderPainted(true);
        BotonNum4.setBounds(a-(TamañoBoton*2),b-(TamañoBoton/2) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla -190,-50
        Component add4 = add(BotonNum4);
        VentanaPrincipalMatricula.add(add4);
        
        BotonNum5.setBorderPainted(true);
        BotonNum5.setBounds(a-TamañoBoton,b-TamañoBoton/2 , TamañoBoton, TamañoBoton);//-50,-50
        Component add5 = add(BotonNum5);
        VentanaPrincipalMatricula.add(add5);
        
        BotonNum6.setBorderPainted(true);
        BotonNum6.setBounds(a,b-(TamañoBoton/2) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla +90,+50
        Component add6 = add(BotonNum6);
        VentanaPrincipalMatricula.add(add6);
        
        BotonNum7.setBorderPainted(true);
        BotonNum7.setBounds(a-(TamañoBoton*2),b+(TamañoBoton-(TamañoBoton/2)) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla -190,+90
        Component add7 = add(BotonNum7);
        VentanaPrincipalMatricula.add(add7);
        
        BotonNum9.setBorderPainted(true);
        BotonNum9.setBounds(a,b+(TamañoBoton-(TamañoBoton/2)) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla +90,+90
        Component add9 = add(BotonNum9);
        VentanaPrincipalMatricula.add(add9);
        
        BotonNum8.setBorderPainted(true);
        BotonNum8.setBounds(a-(TamañoBoton),b+(TamañoBoton-(TamañoBoton/2)) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla -50,+90
        Component add8 = add(BotonNum8);
        VentanaPrincipalMatricula.add(add8);

        BotonNum0.setBorderPainted(true);
        BotonNum0.setBounds(a-((TamañoBoton)+TamañoBoton),b+(TamañoBoton+(TamañoBoton-(TamañoBoton/2))) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla  ,+230
        Component add0 = add(BotonNum0);
        VentanaPrincipalMatricula.add(add0);
        
        BotonBorrar.setBorderPainted(true);
        BotonBorrar.setBounds(a-(TamañoBoton),b+(TamañoBoton+(TamañoBoton-(TamañoBoton/2))) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla
        Component addera = add(BotonBorrar);
        VentanaPrincipalMatricula.add(addera);
        
        BotonLimpiar.setBorderPainted(true);
        BotonLimpiar.setBounds(a+(TamañoBoton-(TamañoBoton)),b+(TamañoBoton+(TamañoBoton-(TamañoBoton/2))) , TamañoBoton, TamañoBoton);//x,y,AnchoPantalla,AltoPantalla
        Component addclear = add(BotonLimpiar);
        VentanaPrincipalMatricula.add(addclear);
        
        /////////////////
        //jLabelInfoPrincipal.setBorder(new LineBorder(Color.RED,5));
        jLabelInfoPrincipal.setFont(new Font("Tahoma", Font.BOLD, 20));
        jLabelInfoPrincipal.setBackground(Color.WHITE);
        jLabelInfoPrincipal.setOpaque(true);
        jLabelInfoPrincipal.setText("INTRODUCE TU MATRICULA");
        
        jLabelInfoPrincipal.setForeground(Color.BLUE);
        
        jLabelInfoPrincipal.setBounds(a-(TamañoMatricula), 10, TamañoMatricula*2, 30);
        Component addInfoMatricula = add(this.jLabelInfoPrincipal);
        VentanaPrincipalMatricula.add(addInfoMatricula);
        
        
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMG/Fondo1.jpg")));
        jLabel10.setSize(AnchoPantalla,AltoPantalla);
        Component addFondo = add(this.jLabel10);
        VentanaPrincipalMatricula.add(addFondo);
    }
    
}