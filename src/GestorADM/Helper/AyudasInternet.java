/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author dany_
 */
public class AyudasInternet {
    
    /**
    * Nombre:       EstadoConexion
    * Tipo:         Publico
    * Descripcion:  Metodo que verifica si hay conexión a internet mediante el 
    *               ingreso a una pagina web muy conocida(google).
    * Parametros:   Sin parametros  
     * @return      Dato booleano
    **/
    public boolean EstadoConexion(){
        try {
            URL ruta=new URL("http://www.google.com");
            URLConnection rutaC=ruta.openConnection();
            rutaC.connect();
            return true;
        }catch(IOException e){
            return false;
        }
    }
    
    /**
    * Nombre:       EstadoConexion
    * Tipo:         Publico
    * Descripcion:  Metodo que verifica si hay conexión a internet mediante el 
    *               ingreso a una pagina web ingresada.
    * Parametros:     
     * @param DireccionURL
     * @return      Dato booleano
    **/
    public boolean EstadoConexion(String DireccionURL){
        try {
            URL ruta=new URL(DireccionURL);
            URLConnection rutaC=ruta.openConnection();
            rutaC.connect();
            return true;
        }catch(IOException e){
            return false;
        }
    }

}
