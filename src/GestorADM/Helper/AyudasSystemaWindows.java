/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dany_
 */
public class AyudasSystemaWindows {
    
    /**
    * Nombre:       ApagarEquipo
    * Tipo:         Publico
    * Descripcion:  Metodo que permite apagar el equipo inmediatamente.
    * Parametros:   Sin parametros  
    **/
    public void ApagarEquipo(){
        try {
            Runtime runtime = Runtime.getRuntime();
            Process proc = runtime.exec("shutdown -s -t 0");
        } catch (IOException ex) {
            Logger.getLogger(AyudasSystemaWindows.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
    * Nombre:       ReiniciarEquipo
    * Tipo:         Publico
    * Descripcion:  Metodo que permite reiniciar el equipo inmediatamente.
    * Parametros:   Sin parametros  
    **/
    public void ReiniciarEquipo(){
        try {
            Runtime runtime = Runtime.getRuntime();
            Process proc = runtime.exec("shutdown -r -t 0");
        } catch (IOException ex) {
            Logger.getLogger(AyudasSystemaWindows.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
