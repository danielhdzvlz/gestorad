/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.text.Collator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dany_
 */
public class AyudasKardex {
    String DatosEstadisticos[];
    public String [][] IntersecionKardexGenericoCalificaciones(String Etapas[][],String MateriasPlan[][],String Calificaciones[][]){
        String KardexGenerico[][]=KardexGenerico(Etapas,MateriasPlan);
        int NumCreditosPlan=0,NumUAPPLAN=KardexGenerico.length;
        for(int i=0;i<KardexGenerico.length;i++){
            String x=KardexGenerico[i][3].substring(0, KardexGenerico[i][3].length()-2);
            NumCreditosPlan=NumCreditosPlan+(Integer.parseInt(x));
        }     
        String FormatoCalificaciones[][]= DarFormatoCalificaciones(Calificaciones);
        

        Collator comparador = Collator.getInstance();
        comparador.setStrength(Collator.PRIMARY);
        
        String aux[]=new String[FormatoCalificaciones[0].length];
        String aux2[]=new String[FormatoCalificaciones[0].length];
        for(int i=0;i<FormatoCalificaciones[0].length;i++){aux[i]="";}//arreglo auxiliar
        /*------------- reemplazo los datos en el kardex generico -------------------*/
        for(int i=0;i<KardexGenerico.length;i++){
            for(int n=0;n<FormatoCalificaciones.length;n++){
                if(comparador.compare(KardexGenerico[i][1], FormatoCalificaciones[n][1]) == 0){
                    if(this.EsNumeroInt(FormatoCalificaciones[n][2])){
                        FormatoCalificaciones[n][2]=Float.parseFloat(FormatoCalificaciones[n][2])+"";
                    }
                    KardexGenerico[i][2]=FormatoCalificaciones[n][2];
                    KardexGenerico[i][5]=FormatoCalificaciones[n][5].replaceAll("NIV:", KardexGenerico[i][5]);
                    FormatoCalificaciones[n]=aux;
                }
            }
        }        
        /*---------------Asigno el nivel al que corresponde para evitar ponerla en otro lugar-------------*/
        String MateriasRestantes[][]=MatrizRemoverFilasVacias(FormatoCalificaciones);       
        for(int i=0;i<MateriasPlan.length;i++){
            for(int n=0;n<MateriasRestantes.length;n++){
                if(comparador.compare(MateriasPlan[i][11], MateriasRestantes[n][1]) == 0){
                    if(this.EsNumeroFloat(MateriasRestantes[n][2])){
                        MateriasRestantes[n][2]=Float.parseFloat(MateriasRestantes[n][2])+"";
                        MateriasRestantes[n][3]=Float.parseFloat(MateriasRestantes[n][3])+"";
                        MateriasRestantes[n][6]="NIV: "+MateriasPlan[i][7];
                    }else{
                        MateriasRestantes[n][2]=MateriasRestantes[n][2];
                        MateriasRestantes[n][3]=Float.parseFloat(MateriasRestantes[n][3])+"";                        
                        MateriasRestantes[n][6]="NIV: "+MateriasPlan[i][7];
                    }
                    
                }
            }
        }   
        /*------------asigna lugar a las materias optaticas que no estan en la carga generica----------------*/
        for(int i=KardexGenerico.length-1;i>=0;i--){
            for(int n=0;n<MateriasRestantes.length;n++){
                if(comparador.compare(KardexGenerico[i][5], MateriasRestantes[n][6]) == 0){//igual a la etapa formativa                         
                    if(comparador.compare(KardexGenerico[i][4], MateriasRestantes[n][4]) == 0){//igual al tipo de materia obl o opt
                        if(comparador.compare(KardexGenerico[i][3], MateriasRestantes[n][3]) == 0){//igual al numero de creditos
                            if(comparador.compare(KardexGenerico[i][2], "---") == 0){                                    
                                KardexGenerico[i][0]=MateriasRestantes[n][0];
                                KardexGenerico[i][1]=MateriasRestantes[n][1];
                                if(this.EsNumeroFloat(MateriasRestantes[n][2])){
                                    KardexGenerico[i][2]=Float.parseFloat(MateriasRestantes[n][2])+"";
                                }else{
                                    KardexGenerico[i][2]=MateriasRestantes[n][2];
                                }
                                
                                KardexGenerico[i][3]=Float.parseFloat(MateriasRestantes[n][3])+"";
                                KardexGenerico[i][4]=MateriasRestantes[n][4];
                                KardexGenerico[i][5]=MateriasRestantes[n][5].replaceAll("NIV:", KardexGenerico[i][5]);
                                MateriasRestantes[n]=aux;
                            }
                        }
                    }                    
                }
            }
        }  
           
        
        KardexGenerico=this.OrdenarStringMatriz(KardexGenerico, 0);
        /*--------------recorre las que no ha cursado------------------*/       
        for(int i=0;i<Etapas.length;i++){
            String IndiceEtapa=Etapas[i][0];   
            for(int m=0;m<KardexGenerico.length;m++){//se repita todas las materias
                for(int n=0;n<KardexGenerico.length;n++){
                    if(!(n==(KardexGenerico.length-1)) ){
                        if(KardexGenerico[n][KardexGenerico[0].length-1].equalsIgnoreCase(IndiceEtapa)){//compara si es de la estapa
                            if(comparador.compare(KardexGenerico[n][0], KardexGenerico[n+1][0]) < 0){//comparar quien es mayor
                                if(KardexGenerico[n][2].equalsIgnoreCase("---") && (comparador.compare(KardexGenerico[n][6], KardexGenerico[n+1][6]) == 0)){//compara si tiene calificacion el espacio
                                    aux2=KardexGenerico[n];
                                    KardexGenerico[n] = KardexGenerico[n+1];
                                    KardexGenerico[n+1] = aux2;                                                               
                                }                        
                            }
                        } 
                    }else{
                        if(comparador.compare(KardexGenerico[KardexGenerico.length-2][0], KardexGenerico[KardexGenerico.length-1][0]) > 0){//comparar quien es mayor
                            if(KardexGenerico[KardexGenerico.length-1][2].equalsIgnoreCase("---") && (comparador.compare(KardexGenerico[KardexGenerico.length-2][6], KardexGenerico[KardexGenerico.length-1][6]) == 0)){//compara si tiene calificacion el espacio
                                aux2=KardexGenerico[KardexGenerico.length-1];
                                KardexGenerico[KardexGenerico.length-1] = KardexGenerico[KardexGenerico.length-2];
                                KardexGenerico[KardexGenerico.length-2] = aux2;                                                               
                            }                        
                        }
                    }
                } 
            }                       
        }
        /*--------------ordena las que no ha cursado---------*/
        for(int m=0;m<KardexGenerico.length;m++){
            for(int i=0;i<KardexGenerico.length;i++){
                if(!(i==(KardexGenerico.length-1)) ){
                    if(KardexGenerico[i][2].equalsIgnoreCase("---") && KardexGenerico[i+1][2].equalsIgnoreCase("---")){
                        if(comparador.compare(KardexGenerico[i][0], KardexGenerico[i+1][0]) > 0){
                            aux2=KardexGenerico[i];
                            KardexGenerico[i]=KardexGenerico[i+1];
                            KardexGenerico[i+1]=aux2;
                        }
                    }
                }else{
                    if(KardexGenerico[KardexGenerico.length-2][2].equalsIgnoreCase("---") && KardexGenerico[KardexGenerico.length-1][2].equalsIgnoreCase("---")){
                        if(comparador.compare(KardexGenerico[KardexGenerico.length-2][0], KardexGenerico[KardexGenerico.length-1][0]) > 0){
                            aux2=KardexGenerico[i];
                            KardexGenerico[i]=KardexGenerico[i+1];
                            KardexGenerico[i+1]=aux2;
                        }
                    }
                }
            }
        }
        /*-------se elimina  la informacion de observaciones de la no cursadas-----*/
        for(int i=0;i<KardexGenerico.length;i++){
            if(KardexGenerico[i][2].equalsIgnoreCase("---")){
                KardexGenerico[i][5]="";                 
            }             
        }
        //promedio
        DatosEstadisticos=ObtenerEstadisticas(KardexGenerico,NumUAPPLAN,NumCreditosPlan);
        //this.DatosEstadisticos[DatosEstadisticos.length-1]=this.Promedio(KardexGenerico);
        return KardexGenerico;
    }
    public String Promedio(String Matriz[][]){
        float PromedioGeneral=0;int NMaterias=0;int creditos=0;int reprobadas=0;
        for(int i=0;i<Matriz.length;i++){
            if(! (Matriz[i][2].equalsIgnoreCase("---") || Matriz[i][2].equalsIgnoreCase("NA")) ){
                creditos=creditos+Integer.parseInt(Matriz[i][3].substring(0, Matriz[i][3].length()-2));
            }
            if(Matriz[i][2].equalsIgnoreCase("ACR")){
                
            }else{
                if(!(Matriz[i][2].equalsIgnoreCase("NA")) ){
                    if(this.EsNumeroFloat(Matriz[i][2])){
                        if(this.EsNumeroFloat(Matriz[i][2])){
                            NMaterias++;
                            Matriz[i][2]=Matriz[i][2].substring(0, Matriz[i][2].length()-2);//CALIFICACION  
                            PromedioGeneral=PromedioGeneral+Integer.parseInt(Matriz[i][2]);
                            if(Integer.parseInt(Matriz[i][2])<7){
                                reprobadas++;
                            }
                        }                
                    }
                }else{
                    reprobadas++;
                }    
            }
        }   
        DatosEstadisticos[4]=creditos+"";
        int NumCreditosPlan=Integer.parseInt(DatosEstadisticos[5]);
        DatosEstadisticos[6]=Float.parseFloat(String.format("%.3f",((float)creditos/NumCreditosPlan)*100 )) +"";
        DatosEstadisticos[1]=reprobadas+"";
        if(DatosEstadisticos[0].equalsIgnoreCase(DatosEstadisticos[2])){
            DatosEstadisticos[3]=DatosEstadisticos[3].substring(0, DatosEstadisticos[3].length()-2);
            DatosEstadisticos[6]=DatosEstadisticos[6].substring(0, DatosEstadisticos[6].length()-2);
        }
        PromedioGeneral=Float.parseFloat(String.format("%.4f",(PromedioGeneral/(NMaterias))));
        return ""+PromedioGeneral;
    }
    public String [][] MatrizRemoverFilasVacias(String Matriz[][]){
        int FilaVacia=0;
        for (String[] Matriz1 : Matriz) {
            if (Matriz1[0].equals("")) {
                FilaVacia++;
            }
        }
        String MatrizAux[][]=new String[(Matriz.length-FilaVacia)][Matriz[0].length];
        int x=0;
        for (String[] Matriz1 : Matriz) {
            if (!Matriz1[0].equals("")) {
                MatrizAux[x] = Matriz1;
                x++;
            }
        }
        return MatrizAux;
    }
    public String [][] DarFormatoCalificaciones(String Calificaciones[][]){
        String Matriz[][]=OrdenarStringMatriz(Calificaciones,5);   
        Matriz=OrdenarStringMatriz(Matriz,1);  //original por nombre       
        String aux[]=new String[Matriz[0].length];
        for(int i=0;i<Matriz[0].length;i++){aux[i]="";}//arreglo auxiliar
        // Posicion: 5	Dato: A.FECHA_EXAMEN
        for(int i=0;i<Matriz.length;i++){
            String[] Formato = Matriz[i][5].substring(0, Matriz[i][5].length()-11).split("-");
            Matriz[i][5] = Formato[2]+"/"+Formato[1]+"/"+Formato[0];
        }
        Collator comparador = Collator.getInstance();        comparador.setStrength(Collator.PRIMARY);
        //Posicion: 2	Dato: A.CALIFICACION
        if(EsNumeroInt(Matriz[Matriz.length-1][2])){Matriz[Matriz.length-1][2]=Float.parseFloat(Matriz[Matriz.length-1][2])+"";}
        Matriz[Matriz.length-1][2]=Matriz[Matriz.length-1][2].replaceAll("NC","NA").replaceAll("S/D", "NA");   
        //Posicion: 3	Dato: A.CREDITOS
        if(EsNumeroInt(Matriz[Matriz.length-1][3])){Matriz[Matriz.length-1][3]=Float.parseFloat(Matriz[Matriz.length-1][3])+"";}
        //Posicion: 4	Dato: A.TIPO_MATERIA
        Matriz[Matriz.length-1][4]=Matriz[Matriz.length-1][4].replaceAll("OBLIGATORIA", "OBL.").replaceAll("OPTATIVA", "OPT."); //tipo  
        //Posicion: 8         Dato: A.TURNO_CURSO_MATERIA
        Matriz[Matriz.length-1][8]=Matriz[Matriz.length-1][8].replaceAll("MATUTINO","MAT.").replaceAll("VESPERTINO","VES.").replaceAll("DISCONTINUO", "DIS."); 
            
        for(int i=0;i<Matriz.length;i++){
            if(i!=Matriz.length-1){
                ///////////////////////////////////////////////////////////////////////////////////
                Matriz[i][1]=Matriz[i][1].replaceAll("MANEJO DE TECNOLOGÍAS DE LA INFORMACIÓN Y DE LA COMUNICACIÓN", "MANEJO DE TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN");
                //Posicion: 2	Dato: A.CALIFICACION
                if(EsNumeroInt(Matriz[i][2])){
                    Matriz[i][2]=Float.parseFloat(Matriz[i][2])+"";
                }
                Matriz[i][2]=Matriz[i][2].replaceAll("NC","NA").replaceAll("S/D", "NA");   
                //Posicion: 3	Dato: A.CREDITOS
                if(EsNumeroInt(Matriz[i][3])){Matriz[i][3]=Float.parseFloat(Matriz[i][3])+"";}
                //Posicion: 4	Dato: A.TIPO_MATERIA
                Matriz[i][4]=Matriz [i][4].replaceAll("OBLIGATORIA", "OBL.").replaceAll("OPTATIVA", "OPT."); //tipo           
                //Posicion: 8         Dato: A.TURNO_CURSO_MATERIA
                Matriz[i][8]=Matriz[i][8].replaceAll("MATUTINO","MAT.").replaceAll("VESPERTINO","VES.").replaceAll("DISCONTINUO", "DIS.");  
                //Posicion: 9         Dato: A.NOMBRE_PLAN_MATERIA
                //Posicion: 10 	  Dato: A.NOMBRE_PLAN_ALUMNO
                Matriz[i][9]=Matriz[i][9].replaceAll("EFI - UAGRO VIRTUAL","ETAPA DE FORM INSTIT - UAGRO VIRTUAL");
                if(comparador.compare(Matriz[i][9], Matriz[i][10]) == 0){
                    Matriz[i][8]=Matriz[i][8]+"";
                }else{
                    Matriz[i][8]=Matriz[i][8]+" "+Matriz[i][9];
                }
                //Posicion: 11	  Dato: A.CVE_APROB_REPROB       
                if(comparador.compare(Matriz[i][1], Matriz[i+1][1]) == 0){//compara si la materia se repite
                    if(comparador.compare(Matriz[i][6], Matriz[i+1][6])==0){//CASO ESPECIAL DE NO DAR DE BAJA UNA MISMA MATERIA
                            if(Matriz[i][11].equalsIgnoreCase("R") && Matriz[i+1][11].equalsIgnoreCase("A")){
                                Matriz[i]=aux;                                                             
                            }else{
                                if(Matriz[i][11].equalsIgnoreCase("A") && Matriz[i+1][11].equalsIgnoreCase("R")){
                                    Matriz[i+1]=aux;                                                             
                                }else{                  
                                    if(this.EsNumeroFloat(Matriz[i][2]) && this.EsNumeroFloat(Matriz[i+1][2])){                                   
                                        int aux1=0,aux2=0;
                                        if(Matriz[i][2].length()==1){
                                            aux1=Integer.parseInt(Matriz[i][2]);
                                        }else{
                                            aux1=Integer.parseInt(Matriz[i][2].substring(0, Matriz[i][2].length()-2));
                                        }
                                        
                                        if(Matriz[i+1][2].length()==1){
                                            aux1=Integer.parseInt(Matriz[i+1][2]);
                                        }else{
                                            String cadena=Matriz[i+1][2];                                            
                                            int numero=cadena.indexOf(".");
                                            if(numero != -1){
                                                aux1=Integer.parseInt(Matriz[i+1][2].substring(0, Matriz[i+1][2].length()-2));
                                            }else{
                                                aux1=Integer.parseInt(Matriz[i+1][2]);
                                            }                                            
                                        }                                        
                                        if(aux1>aux2){
                                            Matriz[i+1]=aux;Matriz[i][2]=Float.parseFloat(Matriz[i][2])+"";
                                        }else{
                                            Matriz[i]=aux;Matriz[i+1][2]=Float.parseFloat(Matriz[i+1][2])+"";
                                        }                                        
                                    }else{
                                        if(this.EsNumeroFloat(Matriz[i][2])){
                                            Matriz[i+1]=aux;Matriz[i][2]=Float.parseFloat(Matriz[i][2])+"";
                                        }
                                        if(this.EsNumeroFloat(Matriz[i][2])){
                                            Matriz[i]=aux;Matriz[i+1][2]=Float.parseFloat(Matriz[i+1][2])+"";
                                        }
                                    }
                                    
                                } 
                            }
                                                       
                    }else{
                        if(comparador.compare(Matriz[i][6], Matriz[i+1][6]) > 0){//compara el periodo del examen 2015A es > 2014A  
                            if(Matriz[i][11].equalsIgnoreCase("R") && Matriz[i+1][11].equalsIgnoreCase("R")){
                                Matriz[i][5]="REPROB. RECURS. "+Matriz[i][5];
                            }else{
                                if(Matriz[i][11].equalsIgnoreCase("A")){//compara si ya aprobo
                                    Matriz[i][5]="RECURS. "+Matriz[i][5];
                                }else{
                                    Matriz[i][5]="REPROB. "+Matriz[i][5];
                                }
                            }
                            Matriz[i+1]=aux;
                        }else{//si es menor                       
                            if(Matriz[i][11].equalsIgnoreCase("R") && Matriz[i+1][11].equalsIgnoreCase("R")){
                                Matriz[i+1][5]="REPROB. RECURS. "+Matriz[i+1][5];                        
                            }else{
                                if(Matriz[i+1][11].equalsIgnoreCase("A")){
                                    Matriz[i+1][5]="RECURS. "+Matriz[i+1][5];
                                }else{
                                    Matriz[i+1][5]="REPROB. "+Matriz[i+1][5];
                                }
                            }
                            Matriz[i]=aux;                                             
                        }
                    }                    
                }else{                   
                    if(Matriz[i][11].equalsIgnoreCase("R")){
                        Matriz[i][5]="REPROB. "+Matriz[i][5];
                    }
                }
                Matriz[i][5]=Matriz[i][5].replaceAll("REPROB. REPROB. ", "REPROB. "); 
                
            }else{
                if(Matriz.length>1){
                    Matriz[Matriz.length-1][1]=Matriz[Matriz.length-1][1].replaceAll("MANEJO DE TECNOLOGÍAS DE LA INFORMACIÓN Y DE LA COMUNICACIÓN", "MANEJO DE TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN");
                    //Posicion: 2	Dato: A.CALIFICACION
                    if(EsNumeroInt(Matriz[Matriz.length-1][2])){
                        Matriz[Matriz.length-1][2]=Float.parseFloat(Matriz[Matriz.length-1][2])+"";
                    }
                    Matriz[Matriz.length-1][2]=Matriz[Matriz.length-1][2].replaceAll("NC","NA").replaceAll("S/D", "NA");   
                    //Posicion: 3	Dato: A.CREDITOS
                    if(EsNumeroInt(Matriz[Matriz.length-1][3])){Matriz[Matriz.length-1][3]=Float.parseFloat(Matriz[Matriz.length-1][3])+"";}
                    //Posicion: 4	Dato: A.TIPO_MATERIA
                     Matriz[Matriz.length-1][4]=Matriz [Matriz.length-1][4].replaceAll("OBLIGATORIA", "OBL.").replaceAll("OPTATIVA", "OPT."); //tipo           
                    //Posicion: 8         Dato: A.TURNO_CURSO_MATERIA
                    Matriz[Matriz.length-1][8]=Matriz[Matriz.length-1][8].replaceAll("MATUTINO","MAT.").replaceAll("VESPERTINO","VES.").replaceAll("DISCONTINUO", "DIS.");  
                    //Posicion: 9         Dato: A.NOMBRE_PLAN_MATERIA
                    //Posicion: 10 	  Dato: A.NOMBRE_PLAN_ALUMNO
                    Matriz[Matriz.length-1][9]=Matriz[Matriz.length-1][9].replaceAll("EFI - UAGRO VIRTUAL","ETAPA DE FORM INSTIT - UAGRO VIRTUAL");
                    if(comparador.compare(Matriz[Matriz.length-1][9], Matriz[Matriz.length-1][10]) == 0){
                        Matriz[Matriz.length-1][8]=Matriz[Matriz.length-1][8]+"";
                    }else{
                        Matriz[Matriz.length-1][8]=Matriz[Matriz.length-1][8]+" "+Matriz[Matriz.length-1][9];
                    }
                    //Posicion: 11	  Dato: A.CVE_APROB_REPROB 
                    if(comparador.compare(Matriz[Matriz.length-1][1], Matriz[Matriz.length-2][1]) == 0){//compara si la materia se repite
                        if(comparador.compare(Matriz[Matriz.length-1][6], Matriz[Matriz.length-2][6]) > 0){//compara el periodo del examen 2015A es > 2014A  
                            if(Matriz[Matriz.length-1][11].equalsIgnoreCase("R") && Matriz[Matriz.length-2][11].equalsIgnoreCase("R")){
                                Matriz[Matriz.length-1][5]="REPROB. RECURS. "+Matriz[Matriz.length-1][5];
                            }else{
                                if(Matriz[Matriz.length-1][11].equalsIgnoreCase("A")){//compara si ya aprobo
                                    Matriz[Matriz.length-1][5]="RECURS. "+Matriz[Matriz.length-1][5];
                                }else{
                                    Matriz[Matriz.length-1][5]="REPROB. "+Matriz[Matriz.length-1][5];
                                }
                            }
                            Matriz[Matriz.length-2]=aux;
                        }else{//si es menor o igual
                            if(Matriz[Matriz.length-1][11].equalsIgnoreCase("R") && Matriz[Matriz.length-2][11].equalsIgnoreCase("R")){
                                Matriz[Matriz.length-2][5]="REPROB. RECURS. "+Matriz[Matriz.length-2][5];                        
                            }else{
                                if(Matriz[Matriz.length-2][11].equalsIgnoreCase("A")){
                                    Matriz[Matriz.length-2][5]="RECURS. "+Matriz[Matriz.length-2][5];
                                }else{
                                    Matriz[Matriz.length-2][5]="REPROB. "+Matriz[Matriz.length-2][5];
                                }
                            }
                            Matriz[Matriz.length-1]=aux;
                        }
                    }else{
                        if(Matriz[Matriz.length-1][11].equalsIgnoreCase("R")){
                            Matriz[Matriz.length-1][5]="REPROB. "+Matriz[Matriz.length-1][5];
                        }
                    }
                }          
                Matriz[Matriz.length-1][5]=Matriz[Matriz.length-1][5].replaceAll("REPROB. REPROB. ", "REPROB. ");
            }                
        }
        
        Matriz=MatrizRemoverFilasVacias(Matriz);
        String MatrizFinal[][]=new String[Matriz.length][7];
        for(int i=0;i<Matriz.length;i++){  
            if(comparador.compare(Matriz[i][9], "ETAPA DE FORM INSTIT - UAGRO VIRTUAL") == 0){
                Matriz[i][5]="UNIVERSIDAD VIRTUAL, "+Matriz[i][5];
            }
            String Observaciones=Matriz[i][5]+", "+Matriz[i][6]+", NIV:, GPO: "+Matriz[i][7]+", TRN: "+Matriz[i][8];
            MatrizFinal[i][0]=Matriz[i][0];
            MatrizFinal[i][1]=Matriz[i][1];
            MatrizFinal[i][2]=Matriz[i][2];
            MatrizFinal[i][3]=Matriz[i][3];
            MatrizFinal[i][4]=Matriz[i][4];
            MatrizFinal[i][5]=Observaciones;
            MatrizFinal[i][6]=Matriz[i][Matriz[0].length-1];
        }            
        return MatrizFinal;        
    }
    public boolean EsNumeroInt(String cadena){
    	try {
            Integer.parseInt(cadena);
            return true;
    	} catch (NumberFormatException e){
            return false;
    	}
    }
    public boolean EsNumeroFloat(String cadena){
    	try {
            if(cadena.length()<3){
                return this.EsNumeroInt(cadena);
            }else{
                cadena=cadena.substring(0, (cadena.length()-2));
                Float.parseFloat(cadena);
                return true;
            }            
    	} catch (NumberFormatException e){
            return this.EsNumeroInt(cadena);            
    	}
    }
    Collator comparador = Collator.getInstance();
        
    public String [][] OrdenarStringMatriz(String Matriz[][],int columna){
        comparador.setStrength(Collator.PRIMARY);
        String aux2[]=null;
        for (int i = 0; i < Matriz.length; i++){
            for (int j = i + 1; j < Matriz.length; j++){
                if(comparador.compare(Matriz[i][columna], Matriz[j][columna]) > 0){
                    //if (Matriz[i][columna].compareTo(Matriz[j][columna]) > 0){                    
                    aux2 = Matriz[i];
                    Matriz[i] = Matriz[j];
                    Matriz[j] = aux2;
                }
            }
        }
        return Matriz;
    }
    public void MostrarMatriz(String Matriz[][],int espacio,boolean indice){
        String espacios="";
        for(int i=0;i<espacio;i++){espacios=espacios+"\t";}
        for(int i=0;i<Matriz.length;i++){
            for(int j=0;j<Matriz[0].length;j++){
                if(indice) System.out.print("["+i+"]["+j+"]:"+Matriz[i][j]+espacios);
                else       System.out.print(Matriz[i][j]+espacios);
            }System.out.println("");
        }
    }
    public String [] ObtenerEstadisticas(String Matriz2[][],int NumUAPPLAN,int NumCreditosPlan){
        String Matriz[][]=Matriz2;
        float PromedioGeneral=0,PorcentajeUAPAprobadas=0,PorcentajeCreditosAprobados=0;
        int NMaterias=0,UAPAprobadas=0,UAPReprobadas=0,CreditosAprobados=0;String aux="";int aux2=0;
        for(int i=0;i<Matriz.length;i++){
            aux=Matriz[i][2];
            if(aux.equalsIgnoreCase("ACR")){            
                NMaterias++;
                UAPAprobadas++;
                CreditosAprobados=CreditosAprobados+Integer.parseInt(Matriz[i][3].substring(0, Matriz[i][3].length()-2));
                Matriz[i][3]=Float.parseFloat(Matriz[i][3])+"";
            }            
            if(this.EsNumeroFloat(aux)){
                aux2=Integer.parseInt(aux.substring(0, aux.length()-2));
                if(aux2>6){                    
                    UAPAprobadas++;
                    PromedioGeneral=PromedioGeneral+aux2;
                    CreditosAprobados=CreditosAprobados+Integer.parseInt(Matriz[i][3].substring(0, Matriz[i][3].length()-2));
                }else{
                    PromedioGeneral=PromedioGeneral+aux2;
                    UAPReprobadas++;
                }
                Matriz[i][2]=Float.parseFloat(Matriz[i][2])+""; 
                Matriz[i][3]=Float.parseFloat(Matriz[i][3])+"";
            }else{
                if(aux.equalsIgnoreCase("NA")){
                    UAPReprobadas++;
                }
            }           
        }        
        
        PromedioGeneral=Float.parseFloat(String.format("%.4f",(PromedioGeneral/(UAPAprobadas+UAPReprobadas-NMaterias))));
        PorcentajeUAPAprobadas=Float.parseFloat(String.format("%.3f",((float)UAPAprobadas/NumUAPPLAN)*100 ));
        PorcentajeCreditosAprobados=Float.parseFloat(String.format("%.3f",((float)CreditosAprobados/NumCreditosPlan)*100 ));
        String Estadisticas[]={(UAPAprobadas+""),(UAPReprobadas+""),(NumUAPPLAN+""),(PorcentajeUAPAprobadas+""),(CreditosAprobados+""),(NumCreditosPlan+""),(PorcentajeCreditosAprobados+""),(PromedioGeneral+"")};
        return Estadisticas;
    }
    public String[][] KardexGenerico(String Etapas[][],String UAPS[][]){
        int NumElectivas=0,AuxDes=0,MateriasMinimo=Integer.parseInt(UAPS[0][6]);
        String KardexGenerico[][]=new String[MateriasMinimo][7];        
        for (String[] Etapa : Etapas) {
            int AuxEtapa = Integer.parseInt(Etapa[2]);
            for (String[] UAPS1 : UAPS) {
                if (Etapa[1].equalsIgnoreCase(UAPS1[8]) && AuxEtapa>0) {
                    KardexGenerico[AuxDes][0] = UAPS1[10]; //clave
                    KardexGenerico[AuxDes][1] = UAPS1[11]; //uap
                    KardexGenerico[AuxDes][2]="---";//calficacion
                    KardexGenerico[AuxDes][3] = Float.parseFloat(UAPS1[13]) + ""; //creditos
                    KardexGenerico[AuxDes][4] = UAPS1[12].replaceAll("OBLIGATORIA", "OBL.").replaceAll("OPTATIVA", "OPT."); //tipo
                    KardexGenerico[AuxDes][5] = "NIV: " + UAPS1[7]; //observaciones
                    KardexGenerico[AuxDes][6] =UAPS1[7]; //observaciones
                    AuxDes++;
                    AuxEtapa--;  
                }
                if(comparador.compare(UAPS1[11].substring(0,UAPS1[11].length()-2), "ELECTIVA") == 0){
                    NumElectivas++;
                }
            }
        }
        
        NumElectivas=NumElectivas/Etapas.length;
        //EN EL CASO DE LLEVAR EL PLAN ELECTIVAS
        String Electivas[][]=new String[NumElectivas][7];int IndElec=0;
        for(int i=0;i<UAPS.length;i++){
            if(comparador.compare(UAPS[i][11].substring(0,UAPS[i][11].length()-2), "ELECTIVA") == 0){
                Electivas[IndElec][0] = UAPS[i][10]; //clave
                Electivas[IndElec][1] = UAPS[i][11]; //uap
                Electivas[IndElec][2]="---";//calficacion
                Electivas[IndElec][3] = Float.parseFloat(UAPS[i][13]) + ""; //creditos
                Electivas[IndElec][4] = UAPS[i][12].replaceAll("OBLIGATORIA", "OBL.").replaceAll("OPTATIVA", "OPT."); //tipo
                Electivas[IndElec][5] = "NIV: " + UAPS[i][7]; //observaciones
                Electivas[IndElec][6] =UAPS[i][7]; //observaciones
                IndElec=IndElec+1;
            }
        }
        String aux[]=new String[KardexGenerico[0].length];
        for(int i=0;i<KardexGenerico[0].length;i++){aux[i]="";}
        
        int posEle=0;
        for(int i=KardexGenerico.length-1;i>=0;i--){
            for(int n=0;n<Electivas.length;n++){
                if(comparador.compare(KardexGenerico[i][6], Electivas[n][6]) == 0){//igual a la etapa formativa                    
                    if(comparador.compare(KardexGenerico[i][3], Electivas[n][3]) == 0){//igual al numero de creditos      
                        posEle=i;
                        break;
                    }                                       
                }
            }       
            if(posEle>0){
                break;
            }
        }
        
        for(int n=0;n<Electivas.length;n++){
            if(comparador.compare(KardexGenerico[posEle-n][6], Electivas[n][6]) == 0){//igual a la etapa formativa                    
                if(comparador.compare(KardexGenerico[posEle-n][3], Electivas[n][3]) == 0){//igual al numero de creditos      
                    KardexGenerico[posEle-n][0]=Electivas[n][0];
                    KardexGenerico[posEle-n][1]=Electivas[n][1];
                    KardexGenerico[posEle-n][2]=Electivas[n][2];
                    KardexGenerico[posEle-n][3]=Electivas[n][3];
                    KardexGenerico[posEle-n][4]=Electivas[n][4];
                }                                       
            }
        }
        
        KardexGenerico=this.OrdenarStringMatriz(KardexGenerico, 0);       
        return KardexGenerico;
    }
}
