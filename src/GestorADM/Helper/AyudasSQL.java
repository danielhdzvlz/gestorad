package GestorADM.Helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public final class AyudasSQL {
    public String DatosDB[]=new String[7];
    public Connection Conexion;
    public String NombreDB,Usuario,Password,Host,Port,DriverSQL,JDBC;
  
    public void ConfiguracionDB(String BaseDatos){
        String pathConfigDB=new File ("").getAbsolutePath ();
        pathConfigDB=pathConfigDB.replace((char)92,'/')+"/src/GestorADM/Config/"+BaseDatos+".txt";
        String archivo=pathConfigDB;
        FileReader f = null;
        int i=0;
        try {
            String cadena;
            f = new FileReader(archivo);
            try (BufferedReader b = new BufferedReader(f)) {
                while((cadena = b.readLine())!=null) {
                    cadena=cadena.replaceAll("NombreDB : "+'"', "");
                    cadena=cadena.replaceAll("Usuario : "+'"', "");
                    cadena=cadena.replaceAll("Password : "+'"', "");
                    cadena=cadena.replaceAll("Host : "+'"', "");
                    cadena=cadena.replaceAll("Port : "+'"', "");
                    cadena=cadena.replaceAll("DriverSQL : "+'"', "");
                    cadena=cadena.replaceAll("JDBC : "+'"', "");
                    
                    cadena=cadena.replace('"'+",", "");
                    cadena=cadena.replace('"'+"", "");
                    DatosDB[i]=cadena;i++;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                f.close();
            } catch (IOException ex) {
                Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }      
    }    
    
    public AyudasSQL(String BaseDatos,boolean verificar){
        NombreDB="cajero_uai_damf2";Usuario="root";Password="";Host="localhost";Port="";DriverSQL="com.mysql.jdbc.Driver";JDBC="mysql";
    }
    
    public AyudasSQL(String nombreDB,String usuario,String password,String host,String driverSQL,String jdbc){
        NombreDB=nombreDB;Usuario=usuario;Password=password;Host=host;DriverSQL=driverSQL;JDBC=jdbc;
    }
    
    public AyudasSQL(String BaseDatos){
        this.ConfiguracionDB(BaseDatos);
        NombreDB=DatosDB[0];Usuario=DatosDB[1];Password=DatosDB[2];Host=DatosDB[3];Port=DatosDB[4];DriverSQL=DatosDB[5];JDBC=DatosDB[6];
    }
    
    public boolean AbrirConexion(){
        boolean VerificadorConexion=false;
        try {
            String URL="";
            if(JDBC.equalsIgnoreCase("MySQL")){URL="jdbc:"+this.JDBC+"://"+this.Host+"/"+this.NombreDB;}
            if(JDBC.equalsIgnoreCase("Oracle")){URL="jdbc:"+this.JDBC+":thin:@"+this.Host+":"+this.Port+":"+this.NombreDB;}
            Class.forName(this.DriverSQL);
            Conexion=DriverManager.getConnection(URL, this.Usuario,this.Password);
            System.out.println("Conexion establecida");            VerificadorConexion=true;
        } catch (ClassNotFoundException ex) {
            System.out.println("Problemas con la conexion a la Base de Datos");            VerificadorConexion=false;
        } catch (SQLException ex) {
            System.out.println("No se puede conectar a la base");
        }
        return VerificadorConexion;    }
    
    public boolean CerrarConexion(){
        try{          
            Conexion.close();
            System.out.println("Conexion cerrada");
            return true;
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Problemas al cerrar la conexion a la Base de Datos");
            return false;
        }
    }
    
    public boolean ConsultaSQLExisteMatricula(String Matricula) {
        //String sql="SELECT COUNT(*) AS EXISTE FROM VING1_DATALU A WHERE A.Matricula='"+Matricula+"'";
        String sql="SELECT * FROM (SELECT COUNT(*) AS EXISTE FROM VING1_CALIF_ALUMNO a, VING1_DATALU b WHERE b.MATRICULA='"+Matricula+"' AND b.MATRICULA=a.MATRICULA ) WHERE ROWNUM <= 1";
        ResultSet resultado = null;boolean Dato=false;
        try {            
            Statement sentencia = Conexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(sql);  
            while(resultado.next()){
                if(resultado.getInt("EXISTE")>0){
                    Dato=true;
                }else{
                    Dato=false;                  
                }
            }
        } catch (SQLException e) {}
        return Dato;                        
    }
    
    public String[] ConsultaSQLDirecta(String sql) {
        int i=0,columnas=0;String aux[]=null;ArrayList<Object> Datos= new ArrayList<>();
        ResultSet resultado = null;
        Statement sentencia=null;
        try {            
            sentencia = Conexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(sql);    
        } catch (SQLException e) {}            
        try {
            columnas = resultado.getMetaData().getColumnCount();
            aux=new String[columnas];
            while(resultado.next()){
                for(i=1;i<columnas+1;i++){
                    Datos.add(resultado.getString(i));
                }    
            }
            
            int filas=Datos.size();
            aux=new String[filas];
            int m=0,n=0;String auxDato="";
            for(i=0;i<Datos.size();i++){
                if(Datos.get(i)==null){
                    auxDato="";
                }else{
                    auxDato=Datos.get(i).toString();
                }                    
                aux[i]=auxDato;
            }
            sentencia.close();
        } catch (SQLException ex) {
            Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aux;              
    }
    
    public String[][] ResultSetToArray(ResultSet rs){
        String obj[][]=null; 
        try{
            rs.last();
            ResultSetMetaData rsmd = rs.getMetaData();
            int numCols = rsmd.getColumnCount();
            int numFils =rs.getRow(); 
            obj=new String[numFils][numCols];
            int j = 0;
            rs.beforeFirst();
            while (rs.next()){
                for (int i=1;i<numCols+1;i++){
                    obj[j][i]=rs.getString(i);
                }
                j++;
            }
        }catch(SQLException e){} 
        System.out.println(""+obj.length);
        return obj;
    }
    public String[][] ConsultaSQLDirectaMatriz(String sql) {
        ArrayList<Object> Datos= new ArrayList<>();
        int columnas=0; 
        //ArrayList Datos  =new ArrayList();
        ResultSet resultado = null; Statement sentencia=null;
        try {            
            sentencia = this.Conexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(sql);    
            
        } catch (SQLException e) {
           
        }            
        try {
            columnas = resultado.getMetaData().getColumnCount();
            while(resultado.next()){
                for(int i=1;i<columnas+1;i++){
                    Datos.add(resultado.getString(i));
                }
            }
            sentencia.close();
        } catch (SQLException ex) {
            Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int filas=Datos.size()/columnas;
        String Matriz[][]=new String[filas][columnas];
        int m=0,n=0;String aux="";
        for(int i=0;i<Datos.size();i++){
            if(Datos.get(i)==null){
                aux="";
            }else{
                aux=Datos.get(i).toString();
            }                    
            Matriz[m][n]=aux;
            n++;
            if(n==columnas){
                m++;n=0;
            }   
        }
        return Matriz;
    }
 
    
    /*public static void main(String[]args){
        AyudasSQL a=new AyudasSQL("BaseDatos2");
        a.AbrirConexion();
        System.out.println(""+a.ConsultaSQLExisteMatricula("10075168"));
        a.CerrarConexion();
    }*/
}
