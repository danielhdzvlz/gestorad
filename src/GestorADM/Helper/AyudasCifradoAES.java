/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author dany_
 */
public class AyudasCifradoAES {
    private SecretKey key;       
    private Cipher cipher;  
    private final String algoritmo= "AES";
    private final int keysize=16;
    /**
 * Crea la Llave para encriptar/desencriptar
     * @param value
 */
    public void addKey( String value ){
        byte[] valuebytes = value.getBytes();            
        key = new SecretKeySpec( Arrays.copyOf( valuebytes, keysize ) , algoritmo );      
    }

     /**
 * Metodo para encriptar un texto
     * @param texto
 * @return String texto encriptado
 */
    public String encriptar( String texto ){
        String value="";
        try {
            cipher = Cipher.getInstance( algoritmo );             
            cipher.init( Cipher.ENCRYPT_MODE, key );             
            byte[] textobytes = texto.getBytes();
            byte[] cipherbytes = cipher.doFinal( textobytes );
            value=DatatypeConverter.printBase64Binary(cipherbytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            System.err.println( ex.getMessage() );
        }
        return value;
    }

     /**
 * Metodo para desencriptar un texto
 * @param texto Texto encriptado
 * @return String texto desencriptado
     * @throws java.io.IOException
     * @throws java.security.NoSuchAlgorithmException
     * @throws javax.crypto.NoSuchPaddingException
 */
    public String desencriptar( String texto ) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException{
        String str="";        
        try {
            byte[] decordedValue;decordedValue = Base64.getDecoder().decode(texto);              
            cipher = Cipher.getInstance( algoritmo );            
            cipher.init( Cipher.DECRYPT_MODE, key );
            byte[] cipherbytes = cipher.doFinal(decordedValue);
            str = new String( cipherbytes );                                  
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
            System.err.println( ex.getMessage() );
        }
        return str;
    }

    public String Cifrar(String texto){
        return texto;
    }
    
    /*public static void main(String[]args) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException{
        AyudasCifradoAES sec=new AyudasCifradoAES();
        sec.addKey("1234567891123456");
        System.out.println( "Hola Mundo" );

        System.out.println( " ------------ Encriptado ------------ " );
        String texto = sec.encriptar("Hola Mundo");
        System.out.println( texto );
        
        System.out.println( " ------------ Desencriptado ------------ " );
        System.out.println("sss real:");
        System.out.println(sec.desencriptar(texto));
        System.out.println( sec.desencriptar( "yjBqvxzxb1oLG7+1wJsKwQ==" ) );
        if("yjBqvxzxb1oLG7+1wJsKwQ==".equals(texto)){
            System.out.println("clave igual");
        }else{
            System.out.println("diferente");
        }
        
        
        /**
        String texto="hola";
        String cifrado=c.Cifrar(texto);
        System.out.println("Texto original: "+texto);
        System.out.println("Texto cifrado: "+cifrado);*/
    //}*/
}
