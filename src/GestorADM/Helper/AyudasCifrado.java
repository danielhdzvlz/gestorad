/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author dany_
 */
public class AyudasCifrado {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.security.NoSuchAlgorithmException
     * @throws javax.crypto.NoSuchPaddingException
     */

    private SecretKey key;       
    private Cipher cipher;  
    public String algoritmo0="";
    private final String algoritmo1= "AES";
    private final String algoritmo2= "Blowfish";
    private final int keysize=16;
    /**
 * Crea la Llave para encriptar/desencriptar
     * @param value
 */
    public void addKey( String value ){
        byte[] valuebytes = value.getBytes();            
        key = new SecretKeySpec( Arrays.copyOf( valuebytes, keysize ) , algoritmo0 );      
    }

     /**
 * Metodo para encriptar un texto
     * @param texto
 * @return String texto encriptado
 */
    public String encriptar(String texto ){
        String value="";
        try {
            cipher = Cipher.getInstance( algoritmo0 );             
            cipher.init( Cipher.ENCRYPT_MODE, key );             
            byte[] textobytes = texto.getBytes();
            byte[] cipherbytes = cipher.doFinal( textobytes );
            value=DatatypeConverter.printBase64Binary(cipherbytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            System.err.println( ex.getMessage() );
        }
        return value;
    }

     /**
 * Metodo para desencriptar un texto
 * @param texto Texto encriptado
 * @return String texto desencriptado
     * @throws java.io.IOException
     * @throws java.security.NoSuchAlgorithmException
     * @throws javax.crypto.NoSuchPaddingException
 */
    public String desencriptar( String texto ) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException{
        String str="";        
        try {
            byte[] decordedValue;decordedValue = Base64.getDecoder().decode(texto);              
            cipher = Cipher.getInstance( algoritmo0 );            
            cipher.init( Cipher.DECRYPT_MODE, key );
            byte[] cipherbytes = cipher.doFinal(decordedValue);
            str = new String( cipherbytes );                                  
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
            System.err.println( ex.getMessage() );
        }
        return str;
    }
    
    public String Cifrado(String texto){
        String EncriptadoFinal="";
        this.algoritmo0=this.algoritmo1;
        this.addKey("xiGkMdJRsXf7nd7wCpchRan0");
        EncriptadoFinal=this.encriptar(texto);      
        this.algoritmo0=this.algoritmo2;
        this.addKey("MWgareTCeB5k47vAU2quZSUV");
        EncriptadoFinal=this.encriptar(EncriptadoFinal); 
        return EncriptadoFinal;
    }
    
    public String Descifrado(String texto) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException{
        String DesencriptadoFinal="";
        this.algoritmo0=this.algoritmo2;
        this.addKey("MWgareTCeB5k47vAU2quZSUV");
        DesencriptadoFinal=this.desencriptar(texto);
        this.algoritmo0=this.algoritmo1;
        this.addKey("xiGkMdJRsXf7nd7wCpchRan0");
        DesencriptadoFinal=this.desencriptar(DesencriptadoFinal);         
        return DesencriptadoFinal;
    }
}
