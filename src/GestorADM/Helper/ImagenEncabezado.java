package GestorADM.Helper;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.IOException;

public class ImagenEncabezado extends PdfPageEventHelper{    
    
    private Image imagen;
    
    /**     
     * Constructor de la clase, inicializa la imagen que se utilizara en el membrete
     */
    public ImagenEncabezado(){
        try{
            String DirOrigen=(new File ("").getAbsolutePath()).replace((char)92,'/');
            imagen = Image.getInstance(DirOrigen+"/src/IMG/AguilaUAGro2.jpg");
            imagen.scaleAbsoluteHeight(65f);//alto
            imagen.scaleAbsoluteWidth(55f);//ancho
            imagen.setAbsolutePosition(25, 710);          
        }catch(BadElementException | IOException r){
            System.err.println("Error al leer la imagen");
        }    
    }
    
    public ImagenEncabezado(String URLImagen,float AltoImagen,float AnchoImagen,float PosAbsX,float PosAbsY)    {
        try{
            imagen = Image.getInstance(URLImagen);
            imagen.scaleAbsoluteHeight(AltoImagen);//alto
            imagen.scaleAbsoluteWidth(AnchoImagen);//ancho
            imagen.setAbsolutePosition(25, 710);          
        }catch(BadElementException | IOException r){
            System.err.println("Error al leer la imagen");
        }    
    }
    
    /**
     * Manejador del evento onEndPage, usado para generar el encabezado
     * @param writer
     * @param document
     */
    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        try{            
            document.add(imagen);
         }catch(DocumentException doc){
         }        
     }
}