package GestorADM.Helper;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author dany_
 */
public class AyudasDocumentos{
    
    public static void main(String[]args){
        try {
            AyudasDocumentos aux =new AyudasDocumentos();
            String Matricula=JOptionPane.showInputDialog("Matricula:");
            aux.SeleccionarDocumento(Matricula, 1, 0);
        } catch (IOException | DocumentException ex) {
            Logger.getLogger(AyudasDocumentos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    AyudasSQL DBSACE=new AyudasSQL("BaseDatos2"); 
    AyudasSQL DBLOCAL=new AyudasSQL("BaseDatos");    
    AyudasKardex KardexGenerico=new AyudasKardex();    
    Font EncabezadoFuente12TimesBoldItalic = FontFactory.getFont("Times New Roman", 12, Font.BOLDITALIC);
    Font EncabezadoFuente11TimesBoldItalic = FontFactory.getFont("TimesNewRomanPS-BoldMT", 11, Font.BOLDITALIC);
    Font EncabezadoFuente9_5TimesBold = FontFactory.getFont("Arial", 9.5f, Font.BOLD);
    Font EncabezadoFuente9_5TimesBoldItalic = FontFactory.getFont("Times New Roman", 9.5f, Font.BOLDITALIC);
    Font EncabezadoFuente9_5TimesUnderLine = FontFactory.getFont("Times New Roman", 9.5f, Font.UNDERLINE);
    Font EncabezadoFuente9Times = FontFactory.getFont("Times New Roman", 9);
    Font EncabezadoFuente10Times = FontFactory.getFont("Times New Roman", 10);
    Font EncabezadoFuente11Times = FontFactory.getFont("Times New Roman", 11f);
    Font EncabezadoFuente11TimesBold = FontFactory.getFont("Times New Roman", 11, Font.BOLD);
    Font EncabezadoFuente12_5TimesBold = FontFactory.getFont("Times New Roman", 12.5f, Font.BOLD);
    Font EncabezadoFuente7_5ArialUnderLine = FontFactory.getFont("Arial", 7.5f, Font.UNDERLINE);
  
    public void SeleccionarDocumento(String Matricula,int IdDocumento,int accion) throws IOException, FileNotFoundException, DocumentException{
        String file="";            
        switch(IdDocumento){
            case 0:{
                this.GenerarConstanciaFinal(Matricula);  
                file="Constancia2.pdf";
                break;
            }
            case 1:{
                this.GenerarKardezFinal(Matricula);
                file="Kardex.pdf";               
                break;
            }
        }  
        file=(new File ("").getAbsolutePath()).replace((char)92,'/')+"/src/DocumentosGenerados/"+file;
        switch(accion){
            case 0:{
                Runtime.getRuntime().exec("cmd /c start " +file);
                break;
            }   
            case 1:{
                File fileToPrint = new File(file);
                Desktop.getDesktop().print(fileToPrint);
                break;
            }
        }
    }

    public void GenerarKardezFinal(String Matricula) throws DocumentException, FileNotFoundException, IOException {        
        /*INI CONSULTAS*/    //por el momento no cuento con aceso en mi casa
        this.DBSACE.AbrirConexion();
        String SqlDatosAlumnos="SELECT NOMBRE,MATRICULA,VRS_PLAN,NOMBRE_ESCUELA,NOMBRE_PLAN,ANIO_PLAN,NOMBRE_DIRECTOR FROM VING1_DATALU WHERE MATRICULA='"+Matricula+"'";
        String DatosAlumno[]=this.DBSACE.ConsultaSQLDirecta(SqlDatosAlumnos);    
        String SqlEtapas="SELECT NUMERO_FASE,NOMBRE_FASE,MINIMO_FASE_CURSAR FROM VING1_PLANESFLEX WHERE VRS_PLAN="+DatosAlumno[2]+" GROUP BY NUMERO_FASE,NOMBRE_FASE,MINIMO_FASE_CURSAR ORDER BY NUMERO_FASE";
        String Etapas[][]=this.DBSACE.ConsultaSQLDirectaMatriz(SqlEtapas);        
        String SqlUAPS="SELECT * FROM VING1_PLANESFLEX WHERE VRS_PLAN="+DatosAlumno[2];
        String UAPS[][]=this.DBSACE.ConsultaSQLDirectaMatriz(SqlUAPS);     
        String SqlCalificaciones="SELECT A.CVE_MATERIA,A.NOMBRE_MATERIA,A.CALIFICACION,A.CREDITOS,A.TIPO_MATERIA,A.FECHA_EXAMEN,A.CVE_PERIODO,A.GRUPO_CURSO_MATERIA,A.TURNO_CURSO_MATERIA,A.NOMBRE_PLAN_MATERIA,A.NOMBRE_PLAN_ALUMNO,A.CVE_APROB_REPROB FROM VING1_CALIF_ALUMNO A WHERE A.MATRICULA='"+Matricula+"' AND A.VRS_PLAN_ALUMNO='"+DatosAlumno[2]+"'ORDER BY A.CVE_MATERIA";
        String Calificaciones[][]=this.DBSACE.ConsultaSQLDirectaMatriz(SqlCalificaciones);
        this.DBSACE.CerrarConexion();

        /*FIN CONSULTAS*/
        //DatosPrueba a=new DatosPrueba();DatosPrueba2 b=new DatosPrueba2();
        //String DatosAlumno[]=a.AlumnoDaniel;String Etapas[][]=a.EtapasDiana;String UAPS[][]=a.UAPDiana;String Calificaciones[][]=b.CalificacionesDanielFuturo;        
        //String DatosAlumno[]=a.AlumnoDiana;String Etapas[][]=a.EtapasDiana;String UAPS[][]=a.UAPDiana;String Calificaciones[][]=a.CalificacionesDiana;
        //String DatosAlumno[]=a.AlumnoDaniel;String Etapas[][]=a.EtapasDiana;String UAPS[][]=a.UAPDiana;String Calificaciones[][]=a.CalificacionesDaniel;        
        //String DatosAlumno[]=a.AlumnoSergio;String Etapas[][]=a.EtapasDiana;String UAPS[][]=a.UAPDiana;String Calificaciones[][]=a.CalificacionesSergio;        
        //String DatosAlumno[]=a.AlumnoCinthyaCivil;String Etapas[][]=a.EtapasCinthyaCivil;String UAPS[][]=a.UAPSCinthyaCivil;String Calificaciones[][]=a.CalificacionesCinthyaCivil;        
        //String DatosAlumno[]=a.AlumnoSalvadorCivil;String Etapas[][]=a.EtapasSalvadorCivil;String UAPS[][]=a.UAPSSalvadorCivil;String Calificaciones[][]=a.CalificacionesSalvadorCivil;      
        //String DatosAlumno[]=a.AlumnoGaudencioTopo;String Etapas[][]=a.EtapasGaudencioTopo;String UAPS[][]=a.UAPSGaudencioTopo;String Calificaciones[][]=a.CalificacionesGaudencioTopo;      
        //String DatosAlumno[]=a.AlumnoSilviaConstru;String Etapas[][]=a.EtapasSilviaConstru;String UAPS[][]=a.UAPSSilviaConstru;String Calificaciones[][]=a.CalificacionesSilviaConstru;      
        ///////////////////////
        
        String [][]Kardex=KardexGenerico.IntersecionKardexGenericoCalificaciones(Etapas, UAPS, Calificaciones);
        
        /*------------------------inicia el llenado del formato--------------*/
        String AuxiliarString[]=DatosAlumno[0].split("/");DatosAlumno[0]=AuxiliarString[1]+" "+AuxiliarString[0];        
        DatosAlumno[0]=DatosAlumno[0].replace("*", " ").replace("/", " ");
        DatosAlumno[4]=DatosAlumno[4].replaceAll("DE LA CARRERA DE ", "");
        int TamNombre=DatosAlumno[0].length();
        String l[]=DatosAlumno[0].split(" ");
        int inter=0;
        if(TamNombre<20 && TamNombre>=10){inter=95;}
        if(TamNombre<30 && TamNombre>=20){inter=85;}
        if(TamNombre<40 && TamNombre>=30){inter=75;}
        if(TamNombre<85){for(int i=0;i<(inter-TamNombre);i++){DatosAlumno[0]=DatosAlumno[0]+" ";}}    
        
        //30 45             //32f;//30 46       //60                 //0
        float left = 30f;   float right =25f;   float top = 5f;      float bottom = 36;   
        
        Document document = new Document(PageSize.LETTER, left, right, top, bottom);
        
        String DirFile=(new File ("").getAbsolutePath()).replace((char)92,'/')+"/src/DocumentosGenerados/Kardex.pdf";
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(DirFile));
        ImagenEncabezado encabezado = new ImagenEncabezado();
        writer.setPageEvent(encabezado);
        document.open();
        
        
        
        
        FontFactory.registerDirectories();
        Paragraph Universidad = new Paragraph("UNIVERSIDAD  AUTÓNOMA  DE  GUERRERO", FontFactory.getFont("Times New Roman", 14.4f, Font.BOLDITALIC));        Universidad.setAlignment(Element.ALIGN_CENTER);
        Paragraph Unidad = new Paragraph(DatosAlumno[3]+", CHILPANCINGO, GUERRERO ", FontFactory.getFont("Times New Roman", 10f, Font.BOLD));        Unidad.setAlignment(Element.ALIGN_CENTER);
        Paragraph Plan = new Paragraph(DatosAlumno[4]+" - PLAN "+DatosAlumno[5], FontFactory.getFont("Times New Roman", 10f, Font.BOLD));        Plan.setAlignment(Element.ALIGN_CENTER);
        Phrase SubEncabezado = new Phrase("");
        SubEncabezado.add(new Chunk("                                             "+"                              KARDEX DE CALIFICACIONES                                        ", FontFactory.getFont("Times New Roman", 8.7f, Font.BOLD)));
        SubEncabezado.add(new Chunk(this.ObtenerFecha(), FontFactory.getFont("Times New Roman", 8.7f)));
        Paragraph SubEncabezadoAux1 = new Paragraph(SubEncabezado);
        SubEncabezadoAux1.setAlignment(Element.ALIGN_CENTER);
        Phrase SubEncabezado2 = new Phrase("");
        SubEncabezado2.add(new Chunk("NOMBRE DEL ESTUDIANTE     ", FontFactory.getFont("Times New Roman", 8.2f)));
        Chunk underline = new Chunk("  "+DatosAlumno[0],FontFactory.getFont("Times New Roman", 8.7f, Font.BOLD));
        underline.setUnderline(0.8f, -4f); //0.1 thick, -2 y-location
        SubEncabezado2.add(underline);    
        SubEncabezado2.add(new Chunk("             MATRICULA     ", FontFactory.getFont("Times New Roman", 8.2f)));
        Chunk underline2 = new Chunk(DatosAlumno[1]+"                ",FontFactory.getFont("Times New Roman", 10f, Font.BOLD));
        underline2.setUnderline(0.8f, -4f);
        SubEncabezado2.add(underline2);  
        Paragraph SubEncabezadoAux2 = new Paragraph(SubEncabezado2);
        SubEncabezadoAux2.setAlignment(Element.ALIGN_LEFT);  
        
        Paragraph EncabezadoClave = new Paragraph("CLAVE", FontFactory.getFont("Times New Roman", 8.2f));
        EncabezadoClave.setAlignment(Element.ALIGN_CENTER);
        Paragraph EncabezadoUAP = new Paragraph("UNIDAD DE APRENDIZAJE", FontFactory.getFont("Times New Roman", 8.2f));
        EncabezadoUAP.setAlignment(Element.ALIGN_CENTER);
        Paragraph EncabezadoCalif = new Paragraph("CALIF.", FontFactory.getFont("Times New Roman", 8.2f));
        EncabezadoCalif.setAlignment(Element.ALIGN_CENTER);
        Paragraph EncabezadoCred = new Paragraph("CRED.", FontFactory.getFont("Times New Roman", 8.2f));
        EncabezadoCred.setAlignment(Element.ALIGN_CENTER);
        Paragraph EncabezadoObser = new Paragraph("OBSERVACIONES", FontFactory.getFont("Times New Roman", 8.2f));
        EncabezadoObser.setAlignment(Element.ALIGN_CENTER);

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);//AnchoDeLaTabla 105 CON 45 EN LEFT
        table.setWidths(new float[] { .38f,1.82f,.28f,.26f,.21f,2.15f });   //x,x,x, table0.setWidths(new float[] { .38f,1.82f,.28f,.26f,.21f,2.15f });
        
        PdfPCell cell6x = new PdfPCell(new Paragraph(""));cell6x.setHorizontalAlignment(Element.ALIGN_CENTER);cell6x.setColspan(6);cell6x.setBorder(0);
        /*encabezado en tabla*/
        
        table.addCell(cell6x);
        PdfPCell cell1x = new PdfPCell(Universidad);        cell1x.setPaddingBottom(0f);cell1x.setHorizontalAlignment(Element.ALIGN_CENTER);cell1x.setColspan(6);cell1x.setBorder(0);table.addCell(cell1x);
        table.addCell(cell6x);
        PdfPCell cell2x = new PdfPCell(Unidad);             cell2x.setPaddingTop(0.5f);cell2x.setHorizontalAlignment(Element.ALIGN_CENTER);cell2x.setColspan(6);cell2x.setBorder(0);table.addCell(cell2x);
        table.addCell(cell6x);
        PdfPCell cell3x = new PdfPCell(Plan);               cell3x.setPaddingBottom(4.5f);cell3x.setHorizontalAlignment(Element.ALIGN_CENTER);cell3x.setColspan(6);cell3x.setBorder(0);table.addCell(cell3x);
        table.addCell(cell6x);
        PdfPCell cell4x = new PdfPCell(SubEncabezadoAux1);  cell4x.setPaddingBottom(0f);cell4x.setHorizontalAlignment(Element.ALIGN_CENTER);cell4x.setColspan(6);cell4x.setBorder(0);table.addCell(cell4x);        
        table.addCell(cell6x);
        PdfPCell cell5x = new PdfPCell(SubEncabezadoAux2);  cell5x.setPaddingBottom(0f);cell5x.setHorizontalAlignment(Element.ALIGN_LEFT);cell5x.setColspan(6);cell5x.setBorder(0);table.addCell(cell5x); 
        table.addCell(cell6x);     
        /*encabezado en tabla*/
        
        
        PdfPCell cell1 = new PdfPCell(EncabezadoClave);     cell1.setHorizontalAlignment(Element.ALIGN_CENTER);        cell1.setBackgroundColor(BaseColor.LIGHT_GRAY);        table.addCell(cell1);
        PdfPCell cell2 = new PdfPCell(EncabezadoUAP);       cell2.setHorizontalAlignment(Element.ALIGN_CENTER);        cell2.setBackgroundColor(BaseColor.LIGHT_GRAY);        table.addCell(cell2);
        PdfPCell cell3 = new PdfPCell(EncabezadoCalif);     cell3.setHorizontalAlignment(Element.ALIGN_CENTER);        cell3.setBackgroundColor(BaseColor.LIGHT_GRAY);        table.addCell(cell3);        
        PdfPCell cell4 = new PdfPCell(EncabezadoCred);      cell4.setColspan(1);                                        cell4.setHorizontalAlignment(Element.ALIGN_CENTER);        cell4.setBackgroundColor(BaseColor.LIGHT_GRAY);        table.addCell(cell4);        
        PdfPCell cell5 = new PdfPCell();                    cell5.setHorizontalAlignment(Element.ALIGN_CENTER);        cell5.setBackgroundColor(BaseColor.LIGHT_GRAY);        table.addCell(cell5);
        PdfPCell cell6 = new PdfPCell(EncabezadoObser);     cell6.setHorizontalAlignment(Element.ALIGN_CENTER);        cell6.setBackgroundColor(BaseColor.LIGHT_GRAY);        table.addCell(cell6);
        table.setHeaderRows(1);
        table.setHeaderRows(2);
        table.setHeaderRows(3);
        table.setHeaderRows(4);
        table.setHeaderRows(5);
        table.setHeaderRows(6);
        table.setHeaderRows(7);
        table.setHeaderRows(8);
        table.setHeaderRows(9);
        table.setHeaderRows(10);
        table.setHeaderRows(11);
        table.setHeaderRows(12);
        table.setSkipFirstHeader(true);
        
        Collator comparador = Collator.getInstance();
        comparador.setStrength(Collator.PRIMARY);
        for(int i=0;i<Etapas.length;i++){
            Paragraph EtapaAux = new Paragraph(Etapas[i][1], FontFactory.getFont("Times New Roman", 8.1f,Font.BOLD));
            PdfPCell cellEtapaAux = new PdfPCell(EtapaAux);        
            cellEtapaAux.setHorizontalAlignment(Element.ALIGN_LEFT);        
            cellEtapaAux.setColspan(5);        
            cellEtapaAux.setBorder(Rectangle.NO_BORDER);
            Paragraph MinAux = new Paragraph("Min.Cur.: "+Etapas[i][2], FontFactory.getFont("Times New Roman", 8.1f,Font.BOLD));
            PdfPCell cellMinAux = new PdfPCell(MinAux);        
            cellMinAux.setHorizontalAlignment(Element.ALIGN_RIGHT);        
            cellMinAux.setColspan(1);        
            cellMinAux.setBorder(Rectangle.NO_BORDER);
            table.addCell(cellEtapaAux);
            table.addCell(cellMinAux);               
                      float esp=.1f;
            for(int n=0;n<Kardex.length;n++){
                if(Kardex[n][Kardex[0].length-1].equalsIgnoreCase(Etapas[i][0])){//compara si es de la estapa
                    /**/
                    Paragraph p0 = new Paragraph(Kardex[n][0], FontFactory.getFont("Times New Roman", 8.2f));                     
                    PdfPCell cellAux0 = new PdfPCell(p0);                        
                    cellAux0.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellAux0.setPaddingTop(esp);
                    table.addCell(cellAux0);
                    /**/
                    Paragraph p1 = new Paragraph(Kardex[n][1], FontFactory.getFont("Times New Roman", 8.2f));                     
                    PdfPCell cellAux1 = new PdfPCell(p1);                        
                    cellAux1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cellAux1.setPaddingTop(esp);
                    table.addCell(cellAux1);
                    /**/
                    Paragraph p2 = new Paragraph(Kardex[n][2], FontFactory.getFont("Times New Roman", 8.2f));                     
                    PdfPCell cellAux2 = new PdfPCell(p2);                        
                    cellAux2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellAux2.setPaddingTop(esp);
                    table.addCell(cellAux2);
                    /**/
                    Paragraph p3 = new Paragraph(Kardex[n][3], FontFactory.getFont("Times New Roman", 8.2f));                     
                    PdfPCell cellAux3 = new PdfPCell(p3);                        
                    cellAux3.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellAux3.setPaddingTop(esp);
                    table.addCell(cellAux3);
                    /**/
                    Paragraph p4 = new Paragraph(Kardex[n][4], FontFactory.getFont("Times New Roman", 8.2f));                     
                    PdfPCell cellAux4 = new PdfPCell(p4);                        
                    cellAux4.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellAux4.setPaddingTop(esp);
                    table.addCell(cellAux4);
                    /**/
                    Paragraph p5 = new Paragraph(Kardex[n][5], FontFactory.getFont("Times New Roman", 8.2f));                     
                    PdfPCell cellAux5 = new PdfPCell(p5);                        
                    cellAux5.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cellAux5.setPaddingTop(esp);
                    table.addCell(cellAux5);
                        
                } 
            }              
        }         
           
        
        
        String EstAlumn[]=KardexGenerico.DatosEstadisticos;//;{"36","5","55","65.455","264","407","64.865","7.0732"};
        String UAA="UAprendizaje Aprobadas: "+EstAlumn[0];String UAR="UAprendizaje Reprobadas: "+EstAlumn[1];
        String TUPE="Tot. UAprend. del Plan de Estudios: "+EstAlumn[2];String PUA="Porcent. UAprend. Aprob: "+EstAlumn[3];
        String CR="Créditos Aprobados: "+EstAlumn[4];
        String TCPE="Tot. Créditos del Plan de Estudios: "+EstAlumn[5];
        String PCA="Porcent. Créditos Aprob: "+EstAlumn[6];
        String PG="Promedio General............."+EstAlumn[7];
        String NOTA="Solo de Porcentuan UAprend. Aprobadas del Total del Plan de Estudios, las Acreditables NO se promedian";
        
        
        String UAAR=UAA+"             "+UAR;
        String TUPEPUA=TUPE+"       "+PUA;
        String TCPEPCA=TCPE+"       "+PCA;
        String PGNOTA=PG+"                              "+NOTA;

        table.addCell(cell6x);
        PdfPCell cellE1 = new PdfPCell( new Paragraph(UAAR, FontFactory.getFont("Times New Roman", 8.2f)) );        cellE1.setColspan(2);    cellE1.setHorizontalAlignment(Element.ALIGN_LEFT);        cellE1.setBorder(0);        table.addCell(cellE1);
        PdfPCell cellE2 = new PdfPCell( new Paragraph(TUPEPUA, FontFactory.getFont("Times New Roman", 8.2f)) );     cellE2.setColspan(4);    cellE2.setHorizontalAlignment(Element.ALIGN_LEFT);        cellE2.setBorder(0);        table.addCell(cellE2);
        PdfPCell cellE3 = new PdfPCell( new Paragraph(CR, FontFactory.getFont("Times New Roman", 8.2f)) );          cellE3.setColspan(2);    cellE3.setHorizontalAlignment(Element.ALIGN_LEFT);        cellE3.setBorder(0);        table.addCell(cellE3);        
        PdfPCell cellE4 = new PdfPCell( new Paragraph(TCPEPCA, FontFactory.getFont("Times New Roman", 8.2f)) );     cellE4.setColspan(4);    cellE4.setHorizontalAlignment(Element.ALIGN_LEFT);        cellE4.setBorder(0);        table.addCell(cellE4);        
        PdfPCell cellE5 = new PdfPCell( new Paragraph(PGNOTA, FontFactory.getFont("Times New Roman", 8.2f)) );      cellE5.setColspan(6);    cellE5.setHorizontalAlignment(Element.ALIGN_LEFT);        cellE5.setBorder(0);        table.addCell(cellE5);
        table.addCell(cell6x);
        

        
        String Director=DatosAlumno[6];
        String DesarrolloAtt = "A T E N T A M E N T E";
        Paragraph Att = new Paragraph(DesarrolloAtt,FontFactory.getFont("Times New Roman", 9.5f, Font.BOLD));
        Att.setAlignment(Element.ALIGN_CENTER);
        //
        Phrase Director2 = new Phrase("");
        Chunk underline1 = new Chunk(Director, FontFactory.getFont("Times New Roman", 10f, Font.BOLD));
        underline1.setUnderline(0.1f, 10f); //0.1 thick, -2 y-location
        Director2.add(underline1);    
        Paragraph DirectorAux = new Paragraph(Director2);
        DirectorAux.setAlignment(Element.ALIGN_CENTER);
        Att.setSpacingBefore(25f);
        Att.setSpacingAfter(25f);
        
        PdfPCell cellEspacio = new PdfPCell(new Paragraph(" ",FontFactory.getFont("Times New Roman", 25f, Font.BOLD)));cellEspacio.setHorizontalAlignment(Element.ALIGN_CENTER);cellEspacio.setColspan(6);cellEspacio.setBorder(0);
        table.addCell(cellEspacio);
        PdfPCell cellF1 = new PdfPCell(Att);              cellF1.setColspan(6);    cellF1.setHorizontalAlignment(Element.ALIGN_CENTER);        cellF1.setBorder(0);        table.addCell(cellF1);
        table.addCell(cellEspacio);
        PdfPCell cellF2 = new PdfPCell(DirectorAux);      cellF2.setColspan(6);    cellF2.setHorizontalAlignment(Element.ALIGN_CENTER);        cellF2.setBorder(0);        table.addCell(cellF2);
        
        
        document.add(table);        
        document.close();
    }  

    public void GenerarConstancia(String Alumno, String Matricula, String Director, String Semestre, String Grupo, String Turno, String Carrera, String Generacion, String Periodo) throws FileNotFoundException, DocumentException, BadElementException, IOException {
        //Document document = new Document(PageSize.LETTER, 36, 36, 54, 36);
        Periodo=Periodo+".";
        float left = 45f;//30
        float right = 46f;//30
        float top = 25f;//60
        float bottom = 36;//0
        Document document = new Document(PageSize.LETTER, left, right, top, bottom);
        FontFactory.registerDirectories();
        Font a12 = FontFactory.getFont("Times New Roman", 12, Font.BOLDITALIC);
        /*INICIO ENCABEZADO DE LA CONSTANCIA*/
        String EncabezadoTexto1 = "UNIVERSIDAD AUTONOMA DE GUERRERO";
        String EncabezadoTexto2 = "UNIDAD ACADÉMICA DE INGENIERÍA\n";
        String EncabezadoTexto3 = "                         AV.LAZARO CÁRDENAS S/N, CIUDAD UNIVERSITARIA            47-2-79-43                                              CHILPANCINGO,GUERRERO";
        /*FIN ENCABEZADO DE LA CONSTANCIA*/
 /**/
        String Desarrollo1 = "A QUIEN CORRESPONDA:";
        String Desarrollo2 = "EL QUE SUSCRIBE ";
        String Desarrollo3 = " DIRECTOR";
        String Desarrollo4 = "HACE :";
        String Desarrollo5 = "C  O  N  S  T  A  R  :";
        String DesarrolloAtt = "A T E N T A M E N T E";
        String Desarrollo6 = "QUE  EL / LA  C.  ";
        String Desarrollo7 = " ES ALUMNO(A), DE ESTA INSTITUCIÓN EDUCATIVA, Y SE ENCUENTRA LEGALMENTE INSCRITO(A), MATRÍCULA ";
        String Desarrollo8 = " Y ACTUALMENTE ESTÁ CURSANDO EL ";
        String Desarrollo9 = " SEMESTRE EN EL GRUPO ";
        String Desarrollo10 = " TURNO ";
        String Desarrollo11 = " ";
        String Desarrollo12 = " GENERACIÓN ";
        String Desarrollo13 = " PERIODO LECTIVO COMPRENDIDO DEL ";
        String Desarrollo14 = "SE EXTIENDE LA PRESENTE PARA LOS USOS Y FINES LEGALES QUE AL INTERESADO(A) CONVENGA, EN LA CIUDAD DE CHILPANCINGO, ESTADO DE GUERRERO A LOS "+this.ObtenerFechaLetras()+".";
        /**/
 /*Inicio Encabezado Estilos*/
        Paragraph Universidad = new Paragraph(EncabezadoTexto1, FontFactory.getFont("Times New Roman", 11, Font.BOLDITALIC));
        Universidad.setAlignment(Element.ALIGN_CENTER);

        Paragraph Unidad = new Paragraph(EncabezadoTexto2, FontFactory.getFont("Times New Roman", 9.5f, Font.BOLD));
        Unidad.setAlignment(Element.ALIGN_CENTER);

        /*Paragraph Direccion = new Paragraph(EncabezadoTexto3, FontFactory.getFont("Arial", 7.5f, Font.UNDERLINE));
        Direccion.setAlignment(Element.ALIGN_CENTER);
        */
        /**/
        Phrase Direccion2 = new Phrase("");
        Chunk underline2 = new Chunk(EncabezadoTexto3,FontFactory.getFont("Arial", 7.5f, Font.BOLD));
        underline2.setUnderline(0.1f, -2f); //0.1 thick, -2 y-location
        Direccion2.add(underline2);    
        Paragraph Direccion = new Paragraph(Direccion2);
        Direccion.setAlignment(Element.ALIGN_CENTER);
        /**/
        
        /*Final Encabezado Estilos*/
        Paragraph Corresponda = new Paragraph(Desarrollo1, FontFactory.getFont("Times New Roman", 9.7f));
        Corresponda.setAlignment(Element.ALIGN_LEFT);
        Corresponda.setSpacingBefore(30f);
        //
        Phrase Suscribe = new Phrase("");
        Suscribe.add(new Chunk(Desarrollo2, FontFactory.getFont("Times New Roman", 10.7f)));
        Suscribe.add(new Chunk(Director, FontFactory.getFont("Times New Roman", 10.7f, Font.BOLD)));
        Suscribe.add(new Chunk(Desarrollo3, FontFactory.getFont("Times New Roman", 10.7f)));
        Paragraph SuscribeAux = new Paragraph(Suscribe);
        SuscribeAux.setAlignment(Element.ALIGN_RIGHT);
        //
        Paragraph Hace = new Paragraph(Desarrollo4, FontFactory.getFont("Times New Roman", 9.7f));
        Hace.setAlignment(Element.ALIGN_LEFT);
        //
        Paragraph Constar = new Paragraph(Desarrollo5, FontFactory.getFont("Times New Roman", 11.9f, Font.BOLD));
        Constar.setAlignment(Element.ALIGN_CENTER);
        //antes funciona
        Font aux = FontFactory.getFont("Times New Roman", 10.7f);
        Font aux2 = FontFactory.getFont("Times New Roman", 10.7f, Font.BOLD);
        Phrase Legal = new Phrase("");
        Legal.add(new Chunk(Desarrollo6, aux));
        Legal.add(new Chunk(Alumno, aux2));
        Legal.add(new Chunk(Desarrollo7, aux));
        Legal.add(new Chunk(Matricula, aux2));
        Legal.add(new Chunk(Desarrollo8, aux));
        Legal.add(new Chunk(Semestre, aux));
        Legal.add(new Chunk(Desarrollo9, aux));
        Legal.add(new Chunk(Grupo, aux));
        Legal.add(new Chunk(Desarrollo10, aux));
        Legal.add(new Chunk(Turno, aux));
        Legal.add(new Chunk(Desarrollo11, aux));
        Legal.add(new Chunk(Carrera, aux));
        Legal.add(new Chunk(Desarrollo12, aux));
        Legal.add(new Chunk(Generacion, aux));
        Legal.add(new Chunk(Desarrollo13, aux));
        Legal.add(new Chunk(Periodo, aux));
        //
        Paragraph LegalAux = new Paragraph(Legal);
        LegalAux.setAlignment(Element.ALIGN_JUSTIFIED);

        //despues funciona
        Paragraph Extiende = new Paragraph(Desarrollo14, aux);
        Extiende.setAlignment(Element.ALIGN_JUSTIFIED);
        //
        Paragraph Att = new Paragraph(DesarrolloAtt, FontFactory.getFont("Times New Roman", 10.4f, Font.BOLD));
        Att.setAlignment(Element.ALIGN_CENTER);
        //
        Phrase Director2 = new Phrase("");
        Chunk underline = new Chunk(Director,FontFactory.getFont("Times New Roman", 10.4f, Font.BOLD));
        underline.setUnderline(0.1f, 10f); //0.1 thick, -2 y-location
        Director2.add(underline);    
        Paragraph DirectorAux = new Paragraph(Director2);
        DirectorAux.setAlignment(Element.ALIGN_CENTER);
        //
        String DirOrigen=(new File ("").getAbsolutePath()).replace((char)92,'/');
        String DirFile=DirOrigen+"/src/DocumentosGenerados/Constancia.pdf";
        PdfWriter.getInstance(document, new FileOutputStream(DirFile));
        document.open();
        document.setMargins(left, right, 0, bottom);
        /*Encabezado*/
        Universidad.setSpacingAfter(5f);
        document.add(Universidad);
        Unidad.setSpacingAfter(25f);
        document.add(Unidad);
        Direccion.setSpacingAfter(70f);
        document.add(Direccion);
        /*Encabezado*/
        /*Desarrollo*/
        Corresponda.setSpacingAfter(20f);
        document.add(Corresponda);        
        SuscribeAux.setSpacingAfter(10f);
        document.add(SuscribeAux);

        
        Hace.setSpacingAfter(25f);
        document.add(Hace);
        
        Constar.setSpacingAfter(30f);
        document.add(Constar);
        
        LegalAux.setSpacingAfter(40f);
        LegalAux.setLeading(12f);
        document.add(LegalAux);
                
        Extiende.setSpacingAfter(50f);
        Extiende.setLeading(12f);
        document.add(Extiende);
        
        Att.setSpacingAfter(45f);
        document.add(Att);
        
        document.add(DirectorAux);
        
        
        
        
        
        document.close();
        /*Desarrollo*/
        //String DirFile2=(new File ("").getAbsolutePath()).replace((char)92,'/')+"/src/DocumentosGenerados/Constancia2.pdf";
        
        /*Añadiendo la imagen*/
        PdfReader reader = new PdfReader((new File ("").getAbsolutePath()).replace((char)92,'/')+"/src/DocumentosGenerados/Constancia.pdf");
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream((new File ("").getAbsolutePath()).replace((char)92,'/')+"/src/DocumentosGenerados/Constancia2.pdf"));
        Image image = Image.getInstance((new File ("").getAbsolutePath()).replace((char)92,'/')+"/src/IMG/AguilaUAGro.jpg");
        image.scaleAbsoluteHeight(55f);//alto
        image.scaleAbsoluteWidth(50f);//ancho
        PdfImage stream = new PdfImage(image, "", null);
        stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
        PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
        image.setDirectReference(ref.getIndirectReference());
        image.setAbsolutePosition(55, 710);//el primero es en el eje x y el otro al y iniciando en la esquina inferior izquierda
        PdfContentByte over = stamper.getOverContent(1);
        over.addImage(image);
        stamper.close();
        reader.close();
    }
    
    public void GenerarConstanciaFinal(String Matricula){
        try {
            AyudasDocumentos aux=new AyudasDocumentos();
            DBSACE.AbrirConexion();            
            String sql="SELECT * FROM (SELECT b.MATRICULA,b.NOMBRE,b.NOMBRE_ESCUELA,b.NOMBRE_DIRECTOR,b.NOMBRE_PLAN,b.ANIO_PLAN,b.NIVEL,b.GRUPO,a.CVE_PERIODO,c.TOTAL_SEMESTRES,b.NOMBRE_TURNO,b.FECHA_INICIO_PERIODO,b.FECHA_FIN_PERIODO " +
                    "FROM VING1_CALIF_ALUMNO a, VING1_DATALU b, VING1_PLANESFLEX c WHERE b.MATRICULA='"+Matricula+"' AND b.MATRICULA=a.MATRICULA AND c.VRS_PLAN=a.VRS_PLAN_ALUMNO ORDER BY b.FECHA_FIN_PERIODO ) WHERE ROWNUM <= 1";
            String arreglo[]=DBSACE.ConsultaSQLDirecta(sql);
            int columnas=arreglo.length;         
            
            /*Nombre*/
            String AuxiliarString[]=arreglo[1].split("/");
            arreglo[1]=AuxiliarString[1]+" "+AuxiliarString[0];        arreglo[1]=arreglo[1].replace("*", " ");        arreglo[1]=arreglo[1].replace("/", " ");
            
            /*Periodo*/
            arreglo[columnas-2]=arreglo[columnas-2].replaceAll(" 00:00:00.0","");
            arreglo[columnas-1]=arreglo[columnas-1].replaceAll(" 00:00:00.0","");
            String FechaString[]=arreglo[columnas-2].split("-");
            String Fecha1=FechaString[2]+"-"+FechaString[1]+"-"+FechaString[0].substring(2,4);
            FechaString=arreglo[columnas-1].split("-");
            String Fecha2=FechaString[2]+"-"+FechaString[1]+"-"+FechaString[0].substring(2,4);
            arreglo[columnas-1]=aux.ObtenerPeriodo( Fecha1.replace("-", "/"), Fecha2.replace("-", "/"));
            arreglo[columnas-2]=aux.ObtenerPeriodo( Fecha1.replace("-", "/"), Fecha2.replace("-", "/"));
            
            /*Grado*/
            String grado=aux.ObtenerGrado(arreglo[6]);
            arreglo[columnas-1]=grado;
            /*Generacion*/
            arreglo[8]=arreglo[8].replaceAll("B", "");
            arreglo[8]=arreglo[8].replaceAll("A", "");
            arreglo[9]=arreglo[8]+"-"+((Math.incrementExact((long) (Double.parseDouble(arreglo[9])/2)))+Integer.parseInt(arreglo[8]));
       
            DBSACE.CerrarConexion();
            String Alumno=arreglo[1];
            Matricula=arreglo[0];
            String Director=arreglo[3];
            String Semestre=arreglo[12];
            String Grupo=arreglo[7];
            String Turno=arreglo[10];
            String Carrera=arreglo[4];
            String Generacion=arreglo[9]; 
            String Periodo=arreglo[11];
            aux.GenerarConstancia(Alumno,Matricula,Director,Semestre,Grupo,Turno,Carrera,Generacion,Periodo);
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(AyudasDocumentos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String ObtenerFecha(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calendar = Calendar.getInstance();
        String Fecha=(simpleDateFormat.format(calendar.getTime()));
        Fecha=Fecha.replaceFirst("-01-", " / ENERO / ");
        Fecha=Fecha.replaceFirst("-02-", " / FEBRERO / ");
        Fecha=Fecha.replaceFirst("-03-", " / MARZO / ");
        Fecha=Fecha.replaceFirst("-04-", " / ABRIL / ");
        Fecha=Fecha.replaceFirst("-05-", " / MAYO / ");
        Fecha=Fecha.replaceFirst("-06-", " / JUNIO / ");
        Fecha=Fecha.replaceFirst("-07-", " / JULIO / ");
        Fecha=Fecha.replaceFirst("-08-", " / AGOSTO / ");
        Fecha=Fecha.replaceFirst("-09-", " / SEPTIEMBRE / ");
        Fecha=Fecha.replaceFirst("-10-", " / OCTUBRE / ");
        Fecha=Fecha.replaceFirst("-11-", " / NOVIEMBRE / ");
        Fecha=Fecha.replaceFirst("-12-", " / DICIEMBRE / ");
        return Fecha;
    }
    
    public String ObtenerPeriodo(String FechaInicio,String FechaFin){
        String Periodo=FechaInicio;
        Periodo=Periodo.substring(0,Periodo.length()-2);
        Periodo=Periodo.replaceFirst("/01/", " DE ENERO");
        Periodo=Periodo.replaceFirst("/02/", " DE FEBRERO");
        Periodo=Periodo.replaceFirst("/03/", " DE MARZO");
        Periodo=Periodo.replaceFirst("/04/", " DE ABRIL");
        Periodo=Periodo.replaceFirst("/05/", " DE MAYO");
        Periodo=Periodo.replaceFirst("/06/", " DE JUNIO");
        Periodo=Periodo.replaceFirst("/07/", " DE JULIO");
        Periodo=Periodo.replaceFirst("/08/", " DE AGOSTO");
        Periodo=Periodo.replaceFirst("/09/", " DE SEPTIEMBRE");
        Periodo=Periodo.replaceFirst("/10/", " DE OCTUBRE");
        Periodo=Periodo.replaceFirst("/11/", " DE NOVIEMBRE");
        Periodo=Periodo.replaceFirst("/12/", " DE DICIEMBRE");
        Periodo=Periodo+" AL "+FechaFin;
        Periodo=Periodo.replaceFirst("/01/", " DE ENERO DE 20");
        Periodo=Periodo.replaceFirst("/02/", " DE FEBRERO DE 20");
        Periodo=Periodo.replaceFirst("/03/", " DE MARZO DE 20");
        Periodo=Periodo.replaceFirst("/04/", " DE ABRIL DE 20");
        Periodo=Periodo.replaceFirst("/05/", " DE MAYO DE 20");
        Periodo=Periodo.replaceFirst("/06/", " DE JUNIO DE 20");
        Periodo=Periodo.replaceFirst("/07/", " DE JULIO DE 20");
        Periodo=Periodo.replaceFirst("/08/", " DE AGOSTO DE 20");
        Periodo=Periodo.replaceFirst("/09/", " DE SEPTIEMBRE DE 20");
        Periodo=Periodo.replaceFirst("/10/", " DE OCTUBRE DE 20");
        Periodo=Periodo.replaceFirst("/11/", " DE NOVIEMBRE DE 20");
        Periodo=Periodo.replaceFirst("/12/", " DE DICIEMBRE DE 20");
        return Periodo;
    }
    
    public String ObtenerGrado(String Numero){
        String Grado=Numero;
        Grado=Grado.replaceFirst("1", "PRIMERO");
        Grado=Grado.replaceFirst("2", "SEGUNDO");
        Grado=Grado.replaceFirst("3", "TERCERO");
        Grado=Grado.replaceFirst("4", "CUARTO");
        Grado=Grado.replaceFirst("5", "QUINTO");
        Grado=Grado.replaceFirst("6", "SEXTO");
        Grado=Grado.replaceFirst("7", "SEPTIMO");
        Grado=Grado.replaceFirst("8", "OCTAVO");
        Grado=Grado.replaceFirst("9", "NOVENO");
        return Grado;
    }
    
    public String ConvertirNumeroALetras(String Numero){
        String Resultado="";
        String UNIDAD[]="UNO; DOS; TRES; CUATRO; CINCO; SEIS; SIETE; OCHO; NUEVE; DIEZ; ONCE; DOCE; TRECE; CATORCE; QUINCE; DIECISÉIS; DIECISIETE; DIECIOCHO; DIECINUEVE; VEINTE; VEINTIUNO; VEINTIDÓS; VEINTITRÉS; VEINTICUATRO; VEINTICINCO; VEINTISÉIS; VEINTISIETE; VEINTIOCHO; VEINTINUEVE".split("; ");
        int Num=Integer.parseInt(Numero);
        if(Num==20)Resultado=Numero.replaceFirst("20", "VEINTE");
        if(Num==21)Resultado=Numero.replaceFirst("21", "VEINTIUN");
        if(Num==30)Resultado=Numero.replaceFirst("30", "TREINTA");
        if(Num==31)Resultado=Numero.replaceFirst("31", "TREINTAIUN");
        if(Num<20){            Resultado=UNIDAD[Num-1];        }
        if(Num>21 && Num <=29){            Resultado="VEINTI"+UNIDAD[Num-21];        }
        if(Num>2000){
            Num=Num-2000;
            if(Num==20)Resultado=Numero.replaceFirst("2020", "VEINTE");
            if(Num==21)Resultado=Numero.replaceFirst("2021", "VEINTIUNO");
            if(Num==30)Resultado=Numero.replaceFirst("2030", "TREINTA");
            if(Num==31)Resultado=Numero.replaceFirst("2031", "TREINTAIUN0");
            if(Num<20){            Resultado=UNIDAD[Num-1];        }
            if(Num>21 && Num <=29){            Resultado="VEINTI"+UNIDAD[Num-21];        }
            Resultado="DOS MIL "+Resultado;
        }
        return Resultado;
    }
    
    public String ObtenerFechaLetras(){        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calendar = Calendar.getInstance();
        String Fecha=(simpleDateFormat.format(calendar.getTime()));
        String Formato[]=Fecha.split("-");
        String Dia=Formato[0],Mes=Formato[1],Anio=Formato[2];        
        Mes=Mes.replaceFirst("01", " DIAS DEL MES DE ENERO DE ");
        Mes=Mes.replaceFirst("02", " DIAS DEL MES DE FEBRERO DE ");
        Mes=Mes.replaceFirst("03", " DIAS DEL MES DE MARZO DE ");
        Mes=Mes.replaceFirst("04", " DIAS DEL MES DE ABRIL DE ");
        Mes=Mes.replaceFirst("05", " DIAS DEL MES DE MAYO DE ");
        Mes=Mes.replaceFirst("06", " DIAS DEL MES DE JUNIO DE ");
        Mes=Mes.replaceFirst("07", " DIAS DEL MES DE JULIO DE ");
        Mes=Mes.replaceFirst("08", " DIAS DEL MES DE AGOSTO DE ");
        Mes=Mes.replaceFirst("09", " DIAS DEL MES DE SEPTIEMBRE DE ");
        Mes=Mes.replaceFirst("10", " DIAS DEL MES DE OCTUBRE DE ");
        Mes=Mes.replaceFirst("11", " DIAS DEL MES DE NOVIEMBRE DE ");
        Mes=Mes.replaceFirst("12", " DIAS DEL MES DE DICIEMBRE DE ");
        String FechaLetra=this.ConvertirNumeroALetras(Dia)+Mes+this.ConvertirNumeroALetras(Anio);
        return FechaLetra;
    }
    
    public boolean EsNumero(String cadena){
    	try {
            Integer.parseInt(cadena);
            return true;
    	} catch (NumberFormatException e){
            return false;
    	}
    }
    
    public void ELIMINAR_METODO_AUXILIAR_ObtenerDatosMostrar(String Etapas[][],String DatosAlumno[],String UAPS[][],String Calificaciones[][]){
        String cadena="";      String StringDatosAlumno="";
        for (String DatosAlumno1 : DatosAlumno) {
            StringDatosAlumno=StringDatosAlumno+DatosAlumno1+"\n";
        }
        String StringEtapas="";        cadena="";
        for(int i=0;i<Etapas.length;i++){            
            for(int j=0;j<Etapas[0].length;j++){cadena=cadena+Etapas[i][j]+"*";}
            StringEtapas=StringEtapas+cadena+"\n";            cadena="";
        }           cadena="";           String StringMaterias="";
        for(int i=0;i<UAPS.length;i++){            
            for(int j=0;j<UAPS[0].length;j++){cadena=cadena+UAPS[i][j]+"*";}
            StringMaterias=StringMaterias+cadena+"\n";            cadena="";
        }        cadena="";
        String StringCalificaciones="";
        for(int i=0;i<Calificaciones.length;i++){            
            for(int j=0;j<Calificaciones[0].length;j++){cadena=cadena+Calificaciones[i][j]+"*";}
            StringCalificaciones=StringCalificaciones+cadena+"\n";cadena="";
        }        
        String informacion=
                "\n\n\n------------Datos alumnos------------\n"+StringDatosAlumno+
                "\n\n\n------------Etapas plan---------------\n"+StringEtapas+
                "\n\n\n------------Materias plan---------------\n"+StringMaterias+
                "\n\n\n------------calificaciones alumno------------------------\n"+StringCalificaciones;
        System.out.println(informacion);
    }
    
    
    }