/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author dany_
 */
public class AyudasFecha {
    public String ObtenerFechaLetras(){        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calendar = Calendar.getInstance();
        String Fecha=(simpleDateFormat.format(calendar.getTime()));
        String Formato[]=Fecha.split("-");
        String Dia=Formato[0],Mes=Formato[1],Anio=Formato[2];        
        Mes=Mes.replaceFirst("01", " DIAS DEL MES DE ENERO DE ");
        Mes=Mes.replaceFirst("02", " DIAS DEL MES DE FEBRERO DE ");
        Mes=Mes.replaceFirst("03", " DIAS DEL MES DE MARZO DE ");
        Mes=Mes.replaceFirst("04", " DIAS DEL MES DE ABRIL DE ");
        Mes=Mes.replaceFirst("05", " DIAS DEL MES DE MAYO DE ");
        Mes=Mes.replaceFirst("06", " DIAS DEL MES DE JUNIO DE ");
        Mes=Mes.replaceFirst("07", " DIAS DEL MES DE JULIO DE ");
        Mes=Mes.replaceFirst("08", " DIAS DEL MES DE AGOSTO DE ");
        Mes=Mes.replaceFirst("09", " DIAS DEL MES DE SEPTIEMBRE DE ");
        Mes=Mes.replaceFirst("10", " DIAS DEL MES DE OCTUBRE DE ");
        Mes=Mes.replaceFirst("11", " DIAS DEL MES DE NOVIEMBRE DE ");
        Mes=Mes.replaceFirst("12", " DIAS DEL MES DE DICIEMBRE DE ");
        String FechaLetra=this.ConvertirNumeroALetras(Dia)+Mes+this.ConvertirNumeroALetras(Anio);
        return FechaLetra;
    }
    public String ConvertirNumeroALetras(String Numero){
        String Resultado="";
        String UNIDAD[]="UNO; DOS; TRES; CUATRO; CINCO; SEIS; SIETE; OCHO; NUEVE; DIEZ; ONCE; DOCE; TRECE; CATORCE; QUINCE; DIECISÉIS; DIECISIETE; DIECIOCHO; DIECINUEVE; VEINTE; VEINTIUNO; VEINTIDÓS; VEINTITRÉS; VEINTICUATRO; VEINTICINCO; VEINTISÉIS; VEINTISIETE; VEINTIOCHO; VEINTINUEVE".split("; ");
        int Num=Integer.parseInt(Numero);
        if(Num==20)Resultado=Numero.replaceFirst("20", "VEINTE");
        if(Num==21)Resultado=Numero.replaceFirst("21", "VEINTIUN");
        if(Num==30)Resultado=Numero.replaceFirst("30", "TREINTA");
        if(Num==31)Resultado=Numero.replaceFirst("31", "TREINTAIUN");
        if(Num<20){            Resultado=UNIDAD[Num-1];        }
        if(Num>21 && Num <=29){            Resultado="VEINTI"+UNIDAD[Num-21];        }
        if(Num>2000){
            Num=Num-2000;
            if(Num==20)Resultado=Numero.replaceFirst("2020", "VEINTE");
            if(Num==21)Resultado=Numero.replaceFirst("2021", "VEINTIUNO");
            if(Num==30)Resultado=Numero.replaceFirst("2030", "TREINTA");
            if(Num==31)Resultado=Numero.replaceFirst("2031", "TREINTAIUN0");
            if(Num<20){            Resultado=UNIDAD[Num-1];        }
            if(Num>21 && Num <=29){            Resultado="VEINTI"+UNIDAD[Num-21];        }
            Resultado="DOS MIL "+Resultado;
        }
        return Resultado;
    }
    public String ObtenerFecha(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calendar = Calendar.getInstance();
        String Fecha=(simpleDateFormat.format(calendar.getTime()));
        Fecha=Fecha.replaceFirst("-01-", " / ENERO / ");
        Fecha=Fecha.replaceFirst("-02-", " / FEBRERO / ");
        Fecha=Fecha.replaceFirst("-03-", " / MARZO / ");
        Fecha=Fecha.replaceFirst("-04-", " / ABRIL / ");
        Fecha=Fecha.replaceFirst("-05-", " / MAYO / ");
        Fecha=Fecha.replaceFirst("-06-", " / JUNIO / ");
        Fecha=Fecha.replaceFirst("-07-", " / JULIO / ");
        Fecha=Fecha.replaceFirst("-08-", " / AGOSTO / ");
        Fecha=Fecha.replaceFirst("-09-", " / SEPTIEMBRE / ");
        Fecha=Fecha.replaceFirst("-10-", " / OCTUBRE / ");
        Fecha=Fecha.replaceFirst("-11-", " / NOVIEMBRE / ");
        Fecha=Fecha.replaceFirst("-12-", " / DICIEMBRE / ");
        return Fecha;
    }
    public String ObtenerFechaActual(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        String Fecha=(simpleDateFormat.format(calendar.getTime()));
        return Fecha;
    }
    public String ObtenerFechaHoraActual(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String Fecha=(simpleDateFormat.format(calendar.getTime()));
        return Fecha;
    }
    AyudasCifrado cif=new AyudasCifrado(); 
    
    public boolean ValidarLicencia() throws FileNotFoundException, IOException, NoSuchAlgorithmException, NoSuchPaddingException{
        String pathConfigDB=new File ("").getAbsolutePath ();
        pathConfigDB=pathConfigDB.replace((char)92,'/')+"/src/GestorADM/Config/Licencia.txt";
        String archivo=pathConfigDB;
        FileReader f = null;
        File arch = new File (archivo);
        FileReader fr = new FileReader (arch);
        BufferedReader br = new BufferedReader(fr);
        String linea = br.readLine().replaceAll("Serial : ", "");        
        linea=cif.Descifrado(linea);
        String verificar1=linea.substring(0, 10);
        String verificar2=linea.substring(linea.length()-10, linea.length());
        System.out.println(verificar1);
        System.out.println(verificar2);
        if(verificar1.equalsIgnoreCase(verificar2)){
            String Fecha=this.ObtenerFechaActual();
            Collator comparador = Collator.getInstance();        comparador.setStrength(Collator.PRIMARY);
            return comparador.compare(Fecha, linea) < 0;
        }else{
            return false;
        }
    }
    public String CrearLicencia(String FechaLimite){
        String licencia=FechaLimite+"f3a7ef54b73b88d1fc86fe5e089cd40242ec7f57"+FechaLimite;
        licencia=cif.Cifrado(licencia);   
        System.out.println("Serial : "+licencia);
        return licencia;
    }
    public static void main(String[]args){
        try {
            AyudasFecha a=new AyudasFecha();
            a.CrearLicencia("2018-03-30");
            //System.exit(0);
            a.ValidarLicencia();
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
            Logger.getLogger(AyudasFecha.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
