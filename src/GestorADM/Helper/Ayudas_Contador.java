/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dany_
 */
public class Ayudas_Contador {
    Ayudas_Archivo aux=new Ayudas_Archivo();
    AyudasCorreo email=new AyudasCorreo();
    public void Contador(String Documento,String Matricula,String TiempoEjecucion) throws FileNotFoundException{   
        int val = 0;
        try (BufferedReader br = new BufferedReader(new FileReader("Contador.txt"))) {
            String line = null;
            while(true) {
                line = br.readLine();
                if (line == null) break;
                val = Integer.parseInt(line.trim());
            }
        } catch (IOException ex) {
            Logger.getLogger(Ayudas_Contador.class.getName()).log(Level.SEVERE, null, ex);
        }
        val=val+1;        
        long seg=TimeUnit.SECONDS.convert(Long.parseLong(TiempoEjecucion), TimeUnit.NANOSECONDS);
        String mensaje="Num: "+val+",Documento: "+Documento+",Matricula: "+Matricula+",TiempoEjecucion: "+TiempoEjecucion+" nanosegundos = "+seg+" Segundos";
        String generados=aux.LeerArchivo("Documentos.txt");
        generados=generados+"\n"+mensaje;
        aux.EscribirArchivo("Documentos.txt", generados);        
        this.email.EnviarEmail("facultad.de.ingenieria.uagro@gmail.com", "Darknezz13", "facultad.de.ingenieria.uagro@gmail.com", "Contador", generados);
        aux.EscribirArchivo("Contador.txt", val+"");
    }
    public void LogErrores(String Matricula,String Datos) throws FileNotFoundException{   
        String mensaje="Matricula: "+Matricula+",Datos: "+Datos;
        String generados=aux.LeerArchivo("Documentos.txt");
        generados=generados+"\n"+mensaje;
        aux.EscribirArchivo("LogErrores.txt", generados);        
        this.email.EnviarEmail("facultad.de.ingenieria.uagro@gmail.com", "Darknezz13", "facultad.de.ingenieria.uagro@gmail.com", "LogErrores", generados);
    }
    public void Matriculas(String Matricula) throws FileNotFoundException{
        AyudasFecha aux2=new AyudasFecha();     
        String mensaje="Matricula: "+Matricula+",Tiempo: "+aux2.ObtenerFechaHoraActual();
        String generados=aux.LeerArchivo("IngresadasMatriculas.txt");
        generados=generados+"\n"+mensaje;
        aux.EscribirArchivo("IngresadasMatriculas.txt", generados);        
        this.email.EnviarEmail("facultad.de.ingenieria.uagro@gmail.com", "Darknezz13", "facultad.de.ingenieria.uagro@gmail.com", "Matriculas", generados);
    }
}
