/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dany_
 */
public class AyudasArduino {
    public String DatosArduino[]=new String[2];
    public String PuertoSerieCOM,VelocidadComunicacion;
    
    public void ConfiguracionArduino(){
        String pathConfigDB=new File ("").getAbsolutePath ();
        pathConfigDB=pathConfigDB.replace((char)92,'/')+"/src/GestorADM/Config/AceptadorMonedas.txt";
        String archivo=pathConfigDB;
        FileReader f = null;
        int i=0;
        try {
            String cadena;
            f = new FileReader(archivo);
            try (BufferedReader b = new BufferedReader(f)) {
                while((cadena = b.readLine())!=null) {
                    cadena=cadena.replaceAll("PuertoSerieCOM : "+'"', "");
                    cadena=cadena.replaceAll("VelocidadComunicacion : "+'"', "");
                    cadena=cadena.replace('"'+",", "");
                    cadena=cadena.replace('"'+"", "");
                    DatosArduino[i]=cadena;i++;
                }
            }
            PuertoSerieCOM=DatosArduino[0];
            VelocidadComunicacion=DatosArduino[1];
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                f.close();
            } catch (IOException ex) {
                Logger.getLogger(AyudasSQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }      
    }
}
