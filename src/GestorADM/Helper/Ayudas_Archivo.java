/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author dany_
 */
public class Ayudas_Archivo {
    
    public void EscribirArchivo(String URL,String Informacion){
        FileWriter fichero = null;
        PrintWriter pw = null;
        try{
            fichero = new FileWriter(URL);
            pw = new PrintWriter(fichero);
            pw.println(Informacion);
        } catch (IOException e) {
            System.out.println("Error "+this.getClass().getName()+": "+e.getMessage());
        } finally {
            try {
                if (null != fichero)
                    fichero.close();
           } catch (IOException e2) {
                System.out.println("Error "+this.getClass().getName()+": "+e2.getMessage());
           }
        }
    }
    
    public String LeerArchivo(String URL){
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String Salva="";
        try{
            archivo = new File (URL);
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            String linea;
            while((linea=br.readLine())!=null)
                Salva=Salva+linea+"\n";
            }           
        catch(IOException e){
            System.out.println("Error "+this.getClass().getName()+": "+e.getMessage());
        }finally{
            try{                
                if( null != fr ){   
                fr.close();                
            }                  
        }catch (IOException e2){
            System.out.println("Error "+this.getClass().getName()+": "+e2.getMessage());
        }
      }
      return Salva;
    }
}
