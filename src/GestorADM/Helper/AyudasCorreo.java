/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM.Helper;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author dany_
 */
public class AyudasCorreo {
    
    /**
    * Nombre:       EnviarEmail
    * Tipo:         Publico
    * Descripcion:  Metodo que envia un correo a otro correo, este puede ser de 
    *               gmail,hotmail,outlook o live.
    * Parametros:     
     * @param EmailEmisor
     * @param EmailPassword
     * @param EmailReceptor
     * @param NombreAsunto
     * @param Mensaje
    **/
    public void EnviarEmail(String EmailEmisor,String EmailPassword,String EmailReceptor,String NombreAsunto,String Mensaje){
        try{
            Properties props = new Properties();

            int resultado=0;
            resultado = EmailEmisor.indexOf("@outlook.com");
            if(resultado != -1){props.setProperty("mail.smtp.host", "smtp-mail.outlook.com");}
            
            resultado = EmailEmisor.indexOf("@live.com");
            if(resultado != -1){props.setProperty("mail.smtp.host", "smtp-mail.outlook.com");}
            
            resultado = EmailEmisor.indexOf("@hotmail.com");
            if(resultado != -1){props.setProperty("mail.smtp.host", "smtp-mail.outlook.com");}
            
            resultado = EmailEmisor.indexOf("@gmail.com");
            if(resultado != -1){props.setProperty("mail.smtp.host", "smtp.gmail.com");}
            
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", EmailEmisor);
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(EmailEmisor));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(EmailReceptor));
            message.setSubject(NombreAsunto);
            message.setText(Mensaje);

            try (Transport t = session.getTransport("smtp")) {
                t.connect(EmailEmisor, EmailPassword);
                t.sendMessage(message, message.getAllRecipients());
            }
        }catch (MessagingException e){}
    }    
}
