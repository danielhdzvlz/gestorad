/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM;

import GestorADM.Helper.AyudasSystemaWindows;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author dany_
 */
public class SistemaControl extends JDialog{
	//AHORA CREAMOS LOS COMPONENTES QUE NECESITAMOS
	JLabel programa = new JLabel("CONTROL DEL SISTEMA", JLabel.CENTER);
	JButton Apagar = new JButton("Apagar");
        JButton Reiniciar = new JButton("Reiniciar");
        JButton aceptar = new JButton("Aceptar");
	//AHORA HACEMOS LOS PANELES QUE NECESITAMOS PARA ACOMODAR NUESTROS COMPONENTES
	JPanel principal = new JPanel(new BorderLayout());
	JPanel info = new JPanel(new GridLayout(4, 1));
	JPanel boton = new JPanel(new FlowLayout()); 
	//CONSTRUCTOR DE LA CLASE
	public SistemaControl(){
        	super(new Frame(), "Acerca de...", true);
                principal.setBackground(Color.WHITE);
                info.setBackground(Color.white);
                boton.setBackground(Color.WHITE);
		//AGREGAMOS AL PANEL info, LAS TRES ETIQUETAS QUE CREAMOS                
		info.add(programa);
		info.add(Apagar);
                info.add(Reiniciar);              
                
		//AGREGAMOS AL PANEL boton, EL BOTON QUE CREAMOS
		boton.add(aceptar);
		//AHORA AGREGAMOS AL PANEL principal, LOS PANELES info, boton
		//QUE A SU VEZ CONTIENEN A TODOS LOS COMPONENTES
		principal.add("Center", info);
		principal.add("South", boton);
		//AGREGAMOS EL PANEL PRINCIPAL AL CUADRO DE DIALOGO
		getContentPane().add(principal);
		//ACOMODAMOS EL TAMA¥O DEL DIALOGO DE ACUERDO AL NUMERO DE COMPONENTES QUE TIENE
		pack();
		//INDICAMOS QUE NO PUEDAN CAMBIAR EL TAMA¥O DEL DIALOGO CON EL MOUSE
		setResizable(false);
		//CENTRAMOS EL DIALOGO EN LA PANTALLA
		Dimension pantalla, cuadro;
		pantalla = Toolkit.getDefaultToolkit().getScreenSize();
                cuadro = this.getSize();
                
		this.setLocation(((pantalla.width - cuadro.width)/2), (pantalla.height - cuadro.height)/2);
		//LE AGREGAMOS EL EVENTO AL BOTON
		aceptar.addActionListener(new ActionListener(){
                        @Override
			public void actionPerformed(ActionEvent evt){
				dispose();
			}
		});
                Apagar.addActionListener(new ActionListener(){
                        @Override
			public void actionPerformed(ActionEvent evt){
				AyudasSystemaWindows aux=new AyudasSystemaWindows();
                                aux.ApagarEquipo();
			}
		});
                Reiniciar.addActionListener(new ActionListener(){
                        @Override
			public void actionPerformed(ActionEvent evt){
				AyudasSystemaWindows aux=new AyudasSystemaWindows();
                                aux.ReiniciarEquipo();
			}
		});
	}//FIN DEL CONSTRUCTOR DE LA CLASE Dialogo
}//FIN DE LA CLASE 
