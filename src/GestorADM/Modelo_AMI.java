/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author dany_
 */
public class Modelo_AMI { 
    
    public boolean MysqlStatus=false;   
    Connection conexion;
    public void AbrirConexion(String usuario,String clave,String baseDatos){
        String url="jdbc:mysql://localhost:3306/"+baseDatos;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(url, usuario,clave);
            MysqlStatus=true;
            System.out.println("Conexion establexida a la db");
        }catch(ClassNotFoundException | SQLException ex){
            Logger.getLogger(Modelo_AMI.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
    //Método para cerrar la conexion de base de datos
    public void CerrarConexion(){
        try{
            conexion.close();
            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion");
        }catch(SQLException ex){
            Logger.getLogger(Modelo_AMI.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
    //Método para obtener datos de la base de datos
    public ResultSet ObtenerDatos(String matricula) {
        String sql="SELECT * FROM personas A WHERE A.Matricula='"+matricula+"';";
        ResultSet resultado;
        try {
            
            Statement sentencia = conexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(sql);
            
        } catch (SQLException e) {
            return null;
        }        return resultado;
    }
    /*public static void main(String[]args){
        Modelo_AMI aux=new Modelo_AMI();
        aux.AbrirConexion("root", "", "biblioteca");
        aux.CerrarConexion();
    }*/
}
