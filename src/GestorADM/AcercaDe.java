/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorADM;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author dany_
 */
public class AcercaDe extends JDialog{
	//AHORA CREAMOS LOS COMPONENTES QUE NECESITAMOS
	JLabel programa = new JLabel("EXPENDEDOR DE DOCUMENTOS ESCOLARES DE LA FACULTAD DE INGENIERIA DE LA UAGRO", JLabel.CENTER);
	JLabel autor = new JLabel("DESARROLLADO POR: DANIEL HERNÁNDEZ VÉLEZ", JLabel.CENTER);
        JLabel modelado = new JLabel("MODELO DEL EXPENDEDOR POR: ADRIAN GUZMAN SALAS", JLabel.CENTER);
        JLabel version = new JLabel("Versión 2.6 Beta(Pruebas)", JLabel.CENTER);
	JLabel derechos = new JLabel("2018, Derechos Reservados", JLabel.CENTER);
	JButton aceptar = new JButton("Aceptar");
	//AHORA HACEMOS LOS PANELES QUE NECESITAMOS PARA ACOMODAR NUESTROS COMPONENTES
	JPanel principal = new JPanel(new BorderLayout());
	JPanel info = new JPanel(new GridLayout(5, 1));
	JPanel boton = new JPanel(new FlowLayout()); 
	//CONSTRUCTOR DE LA CLASE
	public AcercaDe(){
        	super(new Frame(), "Acerca de...", true);
                principal.setBackground(Color.WHITE);
                info.setBackground(Color.white);
                boton.setBackground(Color.WHITE);
		//AGREGAMOS AL PANEL info, LAS TRES ETIQUETAS QUE CREAMOS
		info.add(programa);
		info.add(autor);
                info.add(modelado);
                info.add(version);
		info.add(derechos);
		//AGREGAMOS AL PANEL boton, EL BOTON QUE CREAMOS
		boton.add(aceptar);
		//AHORA AGREGAMOS AL PANEL principal, LOS PANELES info, boton
		//QUE A SU VEZ CONTIENEN A TODOS LOS COMPONENTES
		principal.add("Center", info);
		principal.add("South", boton);
		//AGREGAMOS EL PANEL PRINCIPAL AL CUADRO DE DIALOGO
		getContentPane().add(principal);
		//ACOMODAMOS EL TAMA¥O DEL DIALOGO DE ACUERDO AL NUMERO DE COMPONENTES QUE TIENE
		pack();
		//INDICAMOS QUE NO PUEDAN CAMBIAR EL TAMA¥O DEL DIALOGO CON EL MOUSE
		setResizable(false);
		//CENTRAMOS EL DIALOGO EN LA PANTALLA
		Dimension pantalla, cuadro;
		pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		cuadro = this.getSize();
		this.setLocation(((pantalla.width - cuadro.width)/2), (pantalla.height - cuadro.height)/2);
		//LE AGREGAMOS EL EVENTO AL BOTON
		aceptar.addActionListener(new ActionListener(){
                        @Override
			public void actionPerformed(ActionEvent evt){
				dispose();
			}
		});
	}//FIN DEL CONSTRUCTOR DE LA CLASE Dialogo
}//FIN DE LA CLASE 
