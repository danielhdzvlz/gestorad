/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestorAD;

import GestorADM.Helper.AyudasFecha;
import GestorADM.Vista_AMI;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JOptionPane;

/**
 *
 * @author dany_
 */
public class GestorAD {
    public static void main(String[]args){
        try {
            AyudasFecha Verificador=new AyudasFecha();
            if(Verificador.ValidarLicencia()){
                Vista_AMI a=new Vista_AMI();
                a.setVisible(true);
            }else{
                JOptionPane.showMessageDialog(null, "PERIODO DE PRUEBAS HA TERMINADO");
            }            
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
            Logger.getLogger(GestorAD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
